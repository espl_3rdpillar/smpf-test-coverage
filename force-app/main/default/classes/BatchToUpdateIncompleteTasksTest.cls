@isTest
private class BatchToUpdateIncompleteTasksTest {

    private static void init() {

        List<Month__c> monthList = DIL_TestDataFactory.getMonth(1, true);
        Account_Configuration__c congigObj = new Account_Configuration__c( Number_of_Accounts__c = 3
                                                                          , StartDateTime__c = DateTime.now().addDays(-1)
                                                                          , EndDateTime__c =DateTime.now().addDays(-1).addHours(8)
                                                                          , User__c = UserInfo.getUserId()
                                                                          , Title__c = 'Test Title'
                                                                          , Subject__c = 'Test Subject'
                                                                          , Status__c = 'New'
                                                                          , Name = 'Multiple Accounts 3'
                                                                          , Month__c = monthList[0].Id
                                                                          );
        insert congigObj;
        
        List<Task> taskList = DIL_TestDataFactory.getTasks(1, false);
        taskList[0].Work_Type__c = 'Field Work';
        taskList[0].EndDateTime__c = System.now().addHours(2) - 1;
        taskList[0].StartDateTime__c = System.now() - 1;
        taskList[0].Activity_Status__c = 'Open';
        taskList[0].Type_Of_Activity__c = 'BU Specific Activities';
        taskList[0].Account_Configuration__c = congigObj.Id;
        taskList[0].Month__c = monthList[0].Id;
        insert taskList;
    }

    @isTest static void testIncompleteActivityStatusOrNot() {
        
        init();
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c, Month__c From Task];
        Set<Id> monthIdSet = new Set<Id>();
        for(Task taskObk : lstTask) {
            monthIdSet.add(taskObk.Month__c);
        }
        List<Month__c> monthList = new List<Month__c>();
        for(Month__c monthObj : [SELECT Id, Status__c FROM Month__c WHERE Id IN: monthIdSet]) {
            monthObj.Status__c = 'Approved';
            monthList.add(monthObj);
        }

        update monthList;
        Test.startTest();
            Id batchJobId = Database.executeBatch(new BatchToUpdateIncompleteTasks(), 200);
        Test.stopTest();
        lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];
        System.assert(lstTask.size() > 0);
        System.debug('lstTask[0].Activity_Status__c :: '+lstTask[0].Activity_Status__c);
        System.assertEquals(lstTask[0].Activity_Status__c, 'Incomplete');
    }
    
    @isTest static void scheduleBatchTest() {
        init();
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c, Month__c From Task];
        Set<Id> monthIdSet = new Set<Id>();
        for(Task taskObk : lstTask) {
            monthIdSet.add(taskObk.Month__c);
        }
        List<Month__c> monthList = new List<Month__c>();
        for(Month__c monthObj : [SELECT Id, Status__c FROM Month__c WHERE Id IN: monthIdSet]) {
            monthObj.Status__c = 'Approved';
            monthList.add(monthObj);
        }

        update monthList;
        Test.startTest();
            System.scheduleBatch(new BatchToUpdateIncompleteTasks(), 'BatchToUpdateIncompleteTasks', 5);
            new BatchToUpdateIncompleteTasks().execute(null);
        Test.stopTest();
        lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];
        System.assert(lstTask.size() > 0);
        System.debug('lstTask[0].Activity_Status__c :: '+lstTask[0].Activity_Status__c);
        System.assertEquals(lstTask[0].Activity_Status__c, 'Incomplete');
    }
    
    @isTest static void batchTestForMultipleAccount() {
        
        List<Month__c> monthList = DIL_TestDataFactory.getMonth(1, true);
        Account_Configuration__c congigObj = new Account_Configuration__c( Number_of_Accounts__c = 3
                                                                          , StartDateTime__c = DateTime.now().addDays(-1)
                                                                          , EndDateTime__c =DateTime.now().addDays(-1).addHours(8)
                                                                          , User__c = UserInfo.getUserId()
                                                                          , Title__c = 'Test Title'
                                                                          , Subject__c = 'Test Subject'
                                                                          , Status__c = 'New'
                                                                          , Name = 'Multiple Accounts 3'
                                                                          , Month__c = monthList[0].Id
                                                                          );
        insert congigObj;
        
        List<Task> taskList = DIL_TestDataFactory.getTasks(1, false);
        taskList[0].Work_Type__c = 'Field Work';
        taskList[0].EndDateTime__c = System.now().addHours(2) - 1;
        taskList[0].StartDateTime__c = System.now() - 1;
        taskList[0].Type_Of_Activity__c = 'BU Specific Activities';
        taskList[0].Activity_Status__c = 'Open';
        taskList[0].Account_Configuration__c = congigObj.Id;
        taskList[0].Month__c = monthList[0].Id;
        insert taskList;
        
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c, Month__c From Task];
        Set<Id> monthIdSet = new Set<Id>();
        for(Task taskObk : lstTask) {
            monthIdSet.add(taskObk.Month__c);
            taskObk.Task_Completion_Datetime__c = System.now().addHours(-2) - 1;
        }
        update lstTask;
        
        List<Month__c> monthListToUpdate = new List<Month__c>();
        for(Month__c monthObj : [SELECT Id, Status__c FROM Month__c WHERE Id IN: monthIdSet]) {
            monthObj.Status__c = 'Approved';
            monthListToUpdate.add(monthObj);
        }

        update monthListToUpdate;
        Test.startTest();
            Id batchJobId = Database.executeBatch(new BatchToUpdateIncompleteTasks(), 200);
        Test.stopTest();
        lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];
        Account_Configuration__c accConfig = [select Id, Accounts_Left__c, Number_of_Accounts__c, Accounts_Created__c, Activity_Status__c from Account_Configuration__c limit 1];
        
        System.assertEquals(2 , accConfig.Accounts_Left__c);
        System.assertEquals(1 , accConfig.Accounts_Created__c);
        System.assertEquals('Incomplete' , accConfig.Activity_Status__c);
        System.assert(lstTask.size() > 0);
        System.debug('lstTask[0].Activity_Status__c :: '+lstTask[0].Activity_Status__c);
        System.assertEquals(lstTask[0].Activity_Status__c, 'Complete');
    }

    @isTest static void testEodRecsCreation(){
        
        Start_of_Day__c objSod = new Start_of_Day__c();
        
        objSod.Blowbagets__c = true;
        objSod.Grooming__c = true;
        objSod.Monitors_STT_volume__c = true;
        objSod.Work_Day_Preparation__c = true;
        objSod.Report_to_work_on_time__c = true;
        objSod.Review_open_pending_items__c = true;
        objSod.Tools_and_Kits__c = true;
        
        insert objSod;
        
        Test.setCreatedDate(objSod.Id, DateTime.now().addDays(-1));
        
        Test.startTest();
            BatchToUpdateIncompleteTasks.createEodRecs();
        Test.stopTest();
        
        System.assertEquals(1,Database.countQuery('Select count() From End_of_Day__c'));
    }
    
}
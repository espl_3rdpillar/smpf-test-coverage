/**
 * AUTHOR       : LENNARD PAUL M SANTOS(DELOITTE)
 * DESCRIPTION  : TEST CLASS FOR ORDERITEMTRIGGER.
 * HISTORY      : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                : JUN.16.2016 - CREATED.
 *
**/
@isTest
private class OrderItemTrigger_Test{
    @testSetup static void setupData(){
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        List<Custom_Product__c> parentProductList = new List<Custom_Product__c>();
        Id distributorRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'NATIONAL',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;

        List<Account> accountList = new List<Account>();
        Account distributorAccount = new Account(
            Name = 'TestDistributor',
            AccountNumber = String.valueOf(System.Today().Month()+System.Today().Day()+System.Today().year())+'1', //GAAC: 11/07/2017 - Added Account Number;
            RecordTypeId = distributorRecordTypeId,
            Sales_Organization__c  = 'Feeds',
            General_Trade__c = true,
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true
            );
                    
        Account distributorAccountGFS = new Account(
            Name = 'TestDistributorGFS',
            AccountNumber = String.valueOf(System.Today().Month()+System.Today().Day()+System.Today().year())+'1GFS', //GAAC: 11/07/2017 - Added Account Number;
            RecordTypeId = distributorRecordTypeId,
            Sales_Organization__c  = 'Feeds',
            General_Trade__c = true,
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = false,
            Active__c = true
            );
         
        accountList.add(distributorAccount);   
        accountList.add(distributorAccountGFS);    
        insert accountList;

        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;

        Warehouse__c wr = new Warehouse__c();
        wr.Account__c = distributorAccount.Id;
        wr.Address__c = distributorAccount.Name + distributorAccount.Id;
        wr.Warehouse_No__c = String.valueOf(distributorAccount.Id) + String.valueOf(System.now().millisecond());
        insert wr;
        distributorAccount.Warehouse__c = wr.Id;
        update distributorAccount;
        
        Warehouse__c wrgfs = new Warehouse__c();
        wrgfs.Account__c = distributorAccountGFS.Id;
        wrgfs.Address__c = distributorAccountGFS.Name + distributorAccount.Id;
        wrgfs.Warehouse_No__c = String.valueOf(distributorAccount.Id) + String.valueOf(System.now().millisecond());
        insert wrgfs;
        distributorAccount.Warehouse__c = wrgfs.Id;
        update distributorAccountGFS;

        List<Account> accountForInsertList = new List<Account>();
        Account acc = new Account();
        acc.AccountNumber = String.valueOf(System.Today().Month()+System.Today().Day()+System.Today().year())+'2'; //GAAC: 11/07/2017 - Added Account Number;
        acc.Name = 'Sample Grocery2';
        acc.RecordTypeId = customerRecordTypeId;
        acc.Sales_Organization__c  = 'Feeds';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.Distributor__c = distributorAccount.Id;
        acc.Warehouse__c = wr.Id;
        accountForInsertList.add(acc);

        Account acc2 = new Account();
        acc2.AccountNumber = String.valueOf(System.Today().Month()+System.Today().Day()+System.Today().year())+'3'; //GAAC: 11/07/2017 - Added Account Number;
        acc2.Name = 'Sample Grocery3';
        acc2.RecordTypeId = customerRecordTypeId;
        acc2.Sales_Organization__c  = 'Feeds';
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = distributorAccount.Id;
        acc2.Warehouse__c = wr.Id;
        accountForInsertList.add(acc2);
        
        Account acc3 = new Account();
        acc3.AccountNumber = String.valueOf(System.Today().Month()+System.Today().Day()+System.Today().year())+'3'; //GAAC: 11/07/2017 - Added Account Number;
        acc3.Name = 'Sample Grocery4';
        acc3.RecordTypeId = customerRecordTypeId;
        acc3.Sales_Organization__c  = 'Feeds';
        acc3.BU_Feeds__c = true;
        acc3.Branded__c = true;
        acc3.GFS__c = true;
        acc3.BU_Poultry__c = false;
        acc3.Market__c = marketList.get(0).Id;
        acc3.Distributor__c = distributorAccountGFS.Id;
        acc3.Warehouse__c = wrgfs.Id;
        accountForInsertList.add(acc3);

        insert accountForInsertList;

        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true
            );
        insert settings;
        Custom_Product__c prodParent1 = new Custom_Product__c();
        prodParent1.Name = 'ParentProductFeeds';
        prodParent1.Business_Unit__c = 'Feeds';
        prodParent1.Feeds__c = true;
        prodParent1.Market__c = marketList.get(0).Id;
        prodParent1.SKU_Code__c = '111222333';
        parentProductList.add(prodParent1);

        Custom_Product__c prodParent2 = new Custom_Product__c();
        prodParent2.Name = 'ParentProductGFS';
        prodParent2.Business_Unit__c = 'GFS';
        prodParent2.Feeds__c = true;
        prodParent2.Market__c = marketList.get(1).Id;
        prodParent2.SKU_Code__c = '333444555';
        parentProductList.add(prodParent2);

        if(parentProductList.size()>0){
            System.debug('\n\n\nPARENT PRODUCT LIST : ' + parentProductList + '\n\n\n');
            insert parentProductList;
        }
        List<Conversion__c> ConversionforInsertList = new List<Conversion__c>();
        for(Custom_Product__c customProduct : parentProductList){
            Conversion__c conversionRecord = new Conversion__c(
                Name = 'KG',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecord);
            Conversion__c conversionRecordPC = new Conversion__c(
                Name = 'PC',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecordPC);
        }
        if(ConversionforInsertList.size()>0){
            insert ConversionforInsertList;
        }
    }
    private static testMethod void testInsertOrderItem() {
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        Account distAccount = [SELECT Id FROM Account WHERE Name ='TestDistributor'];
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Custom_Product__c prodSMIS = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductFeeds'];
        Conversion__c uom = [SELECT Id FROM Conversion__c WHERE Product__c = : prodSMIS.Id LIMIT 1];

        Test.startTest();
        Account acc = [SELECT Id FROM Account WHERE Name = 'Sample Grocery2'];

        //GAAC: 11/07/2017 - Insert Order Record with Invoice No, SAS and DSP values;
        //List<Custom_Product__c> prodList = new List<Custom_Product__c>();
        //prodList.add(prodSMIS);

        TestDataFactory tdf = new TestDataFactory();
        list <Custom_Product__c> prodList = tdf.createTestUOMProducts(1, FALSE, TRUE);
        List <Order__c> sif = tdf.createTestTPOrder(1, distAccount.Id, acc.Id);
        List<Order_Item__c> oItem = tdf.createTestUOMOrderItem(prodList, sif[0].Id, 'KG');


        /*Order__c orderRecord = new Order__c(
            Account__c = acc.Id,
            RecordTypeId = actualSalesRecordTypeId
            );
        insert orderRecord;

        Order_Item__c orderItem = new Order_Item__c(
            Order_Form__c = orderRecord.Id,
            Product__c = prodSMIS.Id,
            Conversion__c = uom.Id
            );
        insert orderItem;*/
        //GAAC: 11/07/2017 - END

        Test.stopTest();
    }

    private static testMethod void testUpdateOrderItem() {
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        Account distAccount = [SELECT Id FROM Account WHERE Name ='TestDistributorGFS'];
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Custom_Product__c prodFeeds = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductFeeds'];
        Custom_Product__c prodGFS = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductGFS'];
        Conversion__c uom = [SELECT Id, uom__c FROM Conversion__c WHERE Product__c = : prodGFS.Id LIMIT 1];
        Conversion__c uomFeeds = [SELECT Id FROM Conversion__c WHERE Product__c = : prodFeeds.Id LIMIT 1];
        Test.startTest();
        Account acc = [SELECT Id FROM Account WHERE Name = 'Sample Grocery4'];

        //GAAC: 11/07/2017 - Insert Order Record with Invoice No, SAS and DSP values;
        //List<Custom_Product__c> prodList = new List<Custom_Product__c>();
        //prodList.add(prodFeeds);

        TestDataFactory tdf = new TestDataFactory();
        list <Custom_Product__c> prodList = tdf.createTestUOMProducts(1, FALSE, TRUE);
        List <Order__c> sif = tdf.createTestTPOrder(1, distAccount.Id, acc.Id);
        List<Order_Item__c> oItem = tdf.createTestUOMOrderItem(prodList, sif[0].Id, 'KG');


        //Order_Item__c orderItemForUpdate = [SELECT Id,Product__c FROM Order_Item__c WHERE Id = : orderItem.Id];
        Order_Item__c orderItemForUpdate = [SELECT Id,Product__c FROM Order_Item__c WHERE Id = : oItem[0].Id];
        //GAAC: 11/07/2017 - END
        
        orderItemForUpdate.Product__c = prodGFS.Id;
        orderItemForUpdate.Conversion__c = uom.Id;
        update orderItemForUpdate;
        Test.stopTest();
    }

    private static testMethod void testUpdateReturnOrderItem() {
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        Id returnRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Return Order Form').getRecordTypeId();
        Account distAccount = [SELECT Id FROM Account WHERE Name ='TestDistributorGFS'];
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Custom_Product__c prodFeeds = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductFeeds'];
        Custom_Product__c prodGFS = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductGFS'];
        Conversion__c uom = [SELECT Id FROM Conversion__c WHERE Product__c = : prodGFS.Id LIMIT 1];
        Conversion__c uomFeeds = [SELECT Id FROM Conversion__c WHERE Product__c = : prodFeeds.Id LIMIT 1];
        Test.startTest();
        Account acc = [SELECT Id FROM Account WHERE Name = 'Sample Grocery4'];

        //GAAC: 11/07/2017 - Insert Order Record with Invoice No, SAS and DSP values;

        TestDataFactory tdf = new TestDataFactory();
        list <Custom_Product__c> prodList = tdf.createTestUOMProducts(1, FALSE, TRUE);
        List<Order__c> sif = tdf.createTestTPOrder(2, distAccount.Id, acc.Id);
        List<Order_Item__c> oItem = tdf.createTestUOMOrderItem(prodList, sif[0].Id, 'KG');
        List<Order_Item__c> oRecItem = tdf.createTestUOMOrderItem(prodList, sif[1].Id, 'KG');

        //GAAC: 11/07/2017 - END
        Order_Item__c orderItemForUpdate = [SELECT Id,Product__c FROM Order_Item__c WHERE Id = : oItem[0].Id];
        //GAAC: 11/07/2017 - END

        orderItemForUpdate.Product__c = prodGFS.Id;
        orderItemForUpdate.Conversion__c = uom.Id;
        update orderItemForUpdate;

        List <Order__c> sif2 = tdf.createTestTPChildOrder(1, distAccount.Id, acc.Id, sif[0].Id);
        Order__c orderObjTemp = sif2.get(0);
        orderObjTemp.Invoice_No__c = 'TestIt';
        update orderObjTemp;
        List<Order_Item__c> oItem2 = tdf.createTestUOMOrderItem(prodList, orderObjTemp.Id, 'KG');

        Test.stopTest();
    }

    private static testMethod void testUpdateReturnOrderItem1() {
    
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        Id returnRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Return Order Form').getRecordTypeId();
        Account distAccount = [SELECT Id FROM Account WHERE Name ='TestDistributor'];
        List<Market__c> marketList = [SELECT Id,Name FROM Market__c LIMIT 20];
        Custom_Product__c prodFeeds = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductFeeds'];
        Custom_Product__c prodGFS = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductGFS'];
        Conversion__c uom = [SELECT Id FROM Conversion__c WHERE Product__c = : prodGFS.Id LIMIT 1];
        Conversion__c uomFeeds = [SELECT Id FROM Conversion__c WHERE Product__c = : prodFeeds.Id LIMIT 1];
        Test.startTest();
        Account acc = [SELECT Id FROM Account WHERE Name = 'Sample Grocery3'];

        //GAAC: 11/07/2017 - Insert Order Record with Invoice No, SAS and DSP values;

        TestDataFactory tdf = new TestDataFactory();
        list <Custom_Product__c> prodList = tdf.createTestUOMProducts(1, FALSE, TRUE);
        List<Order__c> sif = tdf.createTestTPOrder(2, distAccount.Id, acc.Id);
        List<Order_Item__c> oItem = tdf.createTestUOMOrderItem(prodList, sif[0].Id, 'KG');
        List<Order_Item__c> oRecItem = tdf.createTestUOMOrderItem(prodList, sif[1].Id, 'KG');
        
        List<Order_Item__c> ordRecForUpdate = new List<Order_Item__c>();
        for(Order_Item__c oIRFU : [SELECT Id,Product__c, Received__c FROM Order_Item__c WHERE Id IN:oRecItem]) {
            oIRFU.Received__c = TRUE; 
            ordRecForUpdate.add(oIRFU);
        }

        Update ordRecForUpdate;
    }
}
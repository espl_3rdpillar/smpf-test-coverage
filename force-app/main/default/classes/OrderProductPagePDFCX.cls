public with sharing class OrderProductPagePDFCX {
    public Order__c currentOrderRec {get;set;}
    public List<Order_Item__c> orderItems  {get;set;}
    public Decimal subtotal {get;set;}
    public Decimal vatAmount  {get;set;}
    public Decimal totalDiscount  {get;set;}
    public Decimal total {get;set;}
    public List<OrderItemsWrapper> orderitemsList {get;set;}
    public string destinationUrl{get;set;}
    public string accountUrl {get;set;}
    Set<id> createdByIdSet = new Set<id>();
    Id currentOrderRecId ;
    
    public OrderProductPagePDFCX(ApexPages.StandardController controller) {
        Integer rowNo = 1;
        Map<id, user> createdByMap = new Map<id,user>();
        orderitemsList = new List<OrderItemsWrapper>();
        OrderItemsWrapper tempOrderItemsWrapper = new OrderItemsWrapper();
        subtotal = 0;
        totalDiscount = 0;
        vatAmount=0;
        total = 0;
        
        orderItems = new List <Order_Item__c>();

        currentOrderRecId = controller.getRecord().Id;

        currentOrderRec = [SELECT Account__r.Name, Order_Date__c,  Distributor_Code__c, Sales_Rep__r.name, 
                Sales_Rep__r.id, DSP1__r.name, DSP1__r.id, Distributor__c, Name, Invoice_No__c,Account__r.AccountNumber,
                Account__r.Business_Unit__c, Account__r.Channel_Type__c, Account__r.Area__c, Account__r.Distributor__r.Distributor_Code__c,
                Order_Total_Discount__c, Invoice_Total_Discount__c, Invoice_Total_Product_Discount_Roll_up__c, Order_Total_Product_Discount_Roll_up__c,
                Invoice_Total_Discount_F__c, Order_Total_Product_Discount_F__c, Order_Total_Discount_F__c

                FROM Order__c 
                WHERE Id = :currentOrderRecId];

        orderItems = [SELECT Id, Name, Invoice_Quantity__c, Invoice_Net_of_Discount__c, Invoice_Gross_Total__c, VAT_Amount__c, CreatedByID,
                Discount_PHP__c, Invoice_Net_Total__c, Product__r.Name, Product__r.Brand__r.name,
                Conversion__r.UOM__r.name, Price__c, Account_Id__c, Invoice_Date__c,Product__r.Category1__r.name,
                Product__r.Category1__r.id, Product__r.Brand__r.id, Product__r.Category1__c, Order_Quantity__c, Product__r.SKU_Code__c,
                Product__r.Business_Unit__c,Order_Discount__c,Order_Total_Product_Discount__c,Invoice_Total_Product_Discount__c


                FROM Order_Item__c
                WHERE Order_Form__c =:currentOrderRec.id ];

        for(Order_Item__c oi: orderItems ){
            createdByIdSet.add(oi.CreatedByID);

            tempOrderItemsWrapper = new OrderItemsWrapper();
            if(oi.Invoice_Net_Total__c>0){
                oi.Invoice_Net_Total__c.setScale(4);
            }
            if(!(oi.Discount_PHP__c>0)){
                oi.Discount_PHP__c=0;
            }
            if(oi.Invoice_Gross_Total__c>0){
                subtotal += oi.Invoice_Gross_Total__c;
            }
            /*if(oi.Discount_PHP__c>0){
                totalDiscount += oi.Discount_PHP__c;
                
            }else{
                oi.Discount_PHP__c = 0;
            }*/
            if(oi.VAT_Amount__c>0){
                vatAmount += oi.VAT_Amount__c;
                
            }

            tempOrderItemsWrapper.orderItemRec = oi;
            tempOrderItemsWrapper.rowNum = rowNo;
            orderitemsList.add(tempOrderItemsWrapper);
            rowNo++;

        }
        
        if(currentOrderRec != null){
            
            if(currentOrderRec.Invoice_Total_Discount_F__c != null && currentOrderRec.Invoice_Total_Discount_F__c != 0){
                totalDiscount = currentOrderRec.Invoice_Total_Discount_F__c;
            }else if(currentOrderRec.Order_Total_Discount_F__c != null && currentOrderRec.Order_Total_Discount_F__c != 0){
                totalDiscount = currentOrderRec.Order_Total_Discount_F__c;
            }
            
            if(currentOrderRec.Invoice_Total_Discount__c != null && currentOrderRec.Invoice_Total_Discount__c != 0){
                totalDiscount = currentOrderRec.Invoice_Total_Discount__c;
            }else if(currentOrderRec.Order_Total_Discount__c != null && currentOrderRec.Order_Total_Discount__c != 0){
                totalDiscount = currentOrderRec.Order_Total_Discount__c;
            }
        }
        
        subtotal = subtotal.setScale(4);
        totalDiscount = totalDiscount.setScale(4);
        vatAmount = vatAmount.setScale(4);
        total = (subtotal - totalDiscount +vatAmount).setScale(4);

        createdByMap = new Map<id, User>([SELECT id, name FROM user WHere Id IN: createdByIdSet]);

        for(OrderItemsWrapper oiw: orderitemsList){
            oiw.createdByName = createdByMap.get(oiw.orderItemRec.CreatedByID).name;
        }

        String sfUrl=URL.getSalesforceBaseUrl().getHost();
        destinationUrl = 'https://'+sfUrl+'/apex/OrderProductPageCSV?id='+ currentOrderRecId;
        accountUrl = 'https://'+sfUrl+'/'+ currentOrderRecId;


    }



    public class OrderItemsWrapper{
        public Order_Item__c orderItemRec {get;set;}
        public Integer rowNum {get;set;}
        public String createdByName {get;set;}
        public OrderItemsWrapper(integer rowNumber,Order_Item__c orderItemRec){
            rowNum = rowNumber;
            orderItemRec = orderItemRec;
        }
        public OrderItemsWrapper(){}
    }



}
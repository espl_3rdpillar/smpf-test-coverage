@isTest
private class TP_OrderSummaryController_Test {

     static testMethod void OSC_test1() {
         
         
        User uRec = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        Id profileId = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'].Id;
        Id roleId = [SELECT Id FROM UserRole WHERE Name =: 'Executive'].Id;
        List<User> userList = new List<User>();
        TestDataFactory tdf = new TestDataFactory();
        List<Account> accList = new List<Account>();
        List<Account> accChildList = new List<Account>();
        List<Contact> newSASCont = new List<Contact>();
        List<Contact> newDSPCont = new List<Contact>();
        List<Ship_Sold_To__c> shspList = new List<Ship_Sold_To__c>();
        List<Custom_Product__c> prodList = new List <Custom_Product__c>();
        List<Custom_Product__c> prodList2 = new List <Custom_Product__c>();
        Custom_Product__c cutProd = new Custom_Product__c();
        
        
        // ======== Create User ======
        for(Integer x=0;x<=5;x++){

            User u = new User(
                ProfileId = profileId,
                LastName = 'smpfctestuser'+ String.valueOf(x),
                Email = 'smpfcTestUser'+ String.valueOf(x) +'@smpfc.com',
                Username = 'smpfcTestUser@smpfc.com' + String.valueOf(x) + System.currentTimeMillis(),
                CompanyName = 'SMPFC',
                Title = 'testTitle',
                Alias = 'smcU',
                TimeZoneSidKey = 'Asia/Manila',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                isActive = true,
                UserRoleId = roleId,
                EmployeeNumber = 'EMP :' + String.valueOf(x)
            );
            userList.add(u);
        }

        insert userlist;
        
        System.runAs(userlist[0]) {
        
            //========= CREATE TRIGGER FLAG CONTROL ======
            TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            Account_CreateWarehouseForDistributor__c = true,
            Account_CreatePricingCondition__c = true,
            Account_PricingConditionName__c = 'National List Price',
            OrderItem_PlantDetermination__c = true,
            OrderItem_AllowEditAndDelete__c = true
            );
            
            insert settings;

            //======= CREATE ORDER SUMMARY SETTINGS =======
            OrderSummarySettings__c orderStng = new OrderSummarySettings__c(name='Received',OrderStatusColumnsFieldSet__c='Received',OpportunityFieldSetName__c='orderDetail_For_Receiving',OLIFieldSetName__c='OrderItems_For_Receiving' , Page_Size__c = 10);
            insert orderStng;

            //======= CREATE LOADING GROUP =======

            Loading_Group__c newLoadGroup = new Loading_Group__c();
            newLoadGroup.Name = 'Premix';
            newLoadGroup.Grouping__c = 'BMC Dry';
            insert newLoadGroup;

            Loading_Group__c newLoadGroup2 = new Loading_Group__c();
            newLoadGroup2.Name = 'MAGNOLIA Milk';
            newLoadGroup2.Grouping__c = 'BMC Wet';
            insert newLoadGroup2;

            //Create Business Unit
            Market__c newMarket = new Market__c();
            newMarket.Name = 'FEEDS';
            newMarket.Active__c = TRUE;
            newMarket.Market_Code__c = '11C';
            insert newMarket;

            Market__c newMarket2 = new Market__c();
            newMarket2.Name = 'BRANDED';
            newMarket2.Active__c = TRUE;
            newMarket2.Market_Code__c = '12C';
            insert newMarket2;

            tdf.createCustomSettings();

            accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
            accChildList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
            //newSASCont = tdf.createContact(5,accList[0].id,'SAS',userList );
            //newDSPCont = tdf.createContact(1,accList[0].id,'Distributor Personnel',userList );
            
            String contDSPRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
            String contSASRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
            
            list <Contact> contList = TestDataFactory.createContact(2, accList[0].Id);
            contList[0].RecordtypeId = contSASRT ;
            contList[1].RecordtypeId = contDSPRT ;
            insert contList;
            
            newSASCont.add(contList[0]);
            newDSPCont.add(contList[1]);

            shspList = tdf.shpSoldToList(accList, TRUE, accChildList[0]);
            prodList =  tdf.createTestProducts(5, NULL, 'Premix', newMarket.id, '');
            prodList2 = new List <Custom_Product__c>();

            cutProd.Name = 'Test Custom Product ' + '-' + System.today() + System.currentTimeMillis();
            cutProd.Active__c = true;
            cutProd.Feeds__c = true;
            cutProd.GFS__c = true;
            cutProd.Poultry__c = true;
            cutProd.Branded__c = true;
            cutProd.Loading_Group_Description__c = 'MAGNOLIA Milk' ;
            cutProd.Material_Type__c = 'Material type';
            cutProd.Market__c = newMarket2.id;
            cutProd.SKU_Code__c = '000009876';
            prodList2.add(cutProd);
            
            system.debug('<<prodList2>>' + prodList);
            system.debug('<<prodList>>' + prodList);
            insert prodList2;     
            
            List<Order__c> orderList = tdf.createTestOrder(1,accList[0].id, newSASCont[0], newDSPCont[0]);
            List<Order_Item__c> ordItemList = tdf.createTestOrderItem(prodList,orderList[0].id, 'Purchase Order');

            List<Order_Item__c> ordUpdateList = new List <Order_Item__c>();
         
            TP_OrderSummaryController tpOrderSummary = new TP_OrderSummaryController();
                tpOrderSummary.performSearch();
                tpOrderSummary.renderAsMethod();
                tpOrderSummary.activeTab_Submit();
                tpOrderSummary.backToListView();
                tpOrderSummary.showOpportunityDetails();
                tpOrderSummary.selectedOrderStat = 'Canceled';
 
              
            TP_OrderSummaryListController tporderList = new TP_OrderSummaryListController(tpOrderSummary);
                tporderList.reloadPage();
                tporderList.doSorting();
                tporderList.Beginning();
                tporderList.Next();
                tporderList.Previous();
                tporderList.printMode = TRUE;
                tporderList.reloadPage();
                tporderList.End();
                tporderList.getDisabledPrevious();
                tporderList.getDisabledNext();
                Integer tolsize = tporderList.getTotal_size();
                Integer tolPgNmbr = tporderList.getPageNumber();
                Integer limitValue;
                Boolean checkLimit;
                String sortBy = 'desc';
                Integer tolPg = tporderList.getTotalPages();

                
            TP_OrderSummaryController tpOrderSummaryB = new TP_OrderSummaryController();
                tpOrderSummaryB.searchText = '*';
                tpOrderSummaryB.performSearch();

            Id oppId = orderList[0].id;
            Order__c ord = orderList[0];

             String brand = cutProd.Brand__c;
             String scale = cutprod.Scale__c;
             String prodGroup =  cutProd.Product_Group__c;
             String prodName = cutProd.Name;
             String matCode = cutProd.SKU_Code__c;
             String itemNumber =  cutProd.SKU_Code__c;
             String uom =  cutProd.Base_UOM__c;
             Decimal decAmt;
             Integer qty;

            SearchWrapper srchwrp = new SearchWrapper(oppId,System.today(),System.today(),'Createddate','DESC');
            OrderDetailandOrderItemsList.oliProdWrapper testWrap = new OrderDetailandOrderItemsList.oliProdWrapper();
            testWrap.brand = brand;
            testWrap.scale = scale;
            testWrap.prodgroup = prodgroup;
            testWrap.prodName = prodName;
            testWrap.matCode = matCode;
            testWrap.itemNumber = itemNumber;
            testWrap.uom = uom; 
            testWrap.qty = qty;
            testWrap.amt =decAmt;

            OrderDetailandOrderItemsList orderdet = new OrderDetailandOrderItemsList();
            List<String> fldApiNms = new List<String>();
            List<String> fldApiPoultNms = new List<String>();

            orderdet.fldApiNms = fldApiNms;
            orderdet.fldApiPoultNms=fldApiPoultNms;
            orderdet.alertClass = '';
            orderdet.alertMessage='';
            orderdet.acclist = accList;
            orderdet.isPoultry = FALSE;
            
            srchwrp.selectedOppId = oppId;  
            srchwrp.oliCsDet = OrderSummarySettings__c.getInstance('Received');
            orderdet.SearchWrapperCls = srchwrp;
            Order__c temOrder = orderdet.opp;
            String stageName = orderdet.stageName;
    
            try {
            orderdet.SaveOppLineItems();
            } catch(exception e){}



        /*OrderSummarySettings__c orderStng = new OrderSummarySettings__c(name='For_Receiving',OrderStatusColumnsFieldSet__c='For_Receiving',OpportunityFieldSetName__c='orderDetail_For_Receiving',OLIFieldSetName__c='OrderItems_For_Receiving');
        insert orderStng;

        SearchWrapper srchwrp = new SearchWrapper('',System.today(),System.today(),'Createddate','DESC');
        srchwrp.selectedOppId = orderList[0].Id;
        srchwrp.oliCsDet = orderStng;
        OrderDetailandOrderItemsList orderdet = new OrderDetailandOrderItemsList();
        orderdet.SearchWrapperCls = srchwrp;
        Order__c ord = orderdet.opp;
        orderdet.SaveOppLineItems();
        String stageName = orderdet.stageName;
        System.assertEquals('For_Receiving', stageName);
       
        /*
        srchwrp.pageSize =  Integer.valueOf(Label.pageSize);
        srchwrp.offsetValue = srchwrp.pageSize + srchwrp.pageSize;
        srchwrp.counter = srchwrp.pageSize +srchwrp.pageSize;
        tporderList.wrapperFromReqParam = srchwrp;
        tporderList.searchWrapperObj = srchwrp;
        //tporderList.count =0;
        System.debug('calling previous');
        tporderList.Previous();
        tporderList.printMode = true;
        tporderList.reloadPage();
        srchwrp.printPDFmode = true;
        //srchwrp.selectedOppId  = null;
        TP_OrderSummaryController tpOrderSummary_1 = new TP_OrderSummaryController();
        Test.setCurrentPageReference(new PageReference('Page.TP_OrderSummary'));
        
        System.currentPageReference().getParameters().put('searchWrapper',json.serialize(srchwrp));
        tpOrderSummary_1.searchWrapperCls = srchwrp;
        tpOrderSummary_1.performSearch();
        tpOrderSummary_1.activeTab_Submit();
        
        
        TP_OrderSummaryListController tporderList_1 = new TP_OrderSummaryListController(tpOrderSummary_1);
        tpOrderSummary_1.activeTab ='For_Receiving';
        TP_OrderSummaryListController tporderList_2 = new TP_OrderSummaryListController(tpOrderSummary_1);
        srchwrp.selectedOppId = null;
        TP_OrderSummaryController tpOrderSummary_3 = new TP_OrderSummaryController();
        tpOrderSummary_3.searchWrapperCls = srchwrp;
        tpOrderSummary_3.performSearch();
        tpOrderSummary_3.activeTab_Submit();
        TP_OrderSummaryListController tporderList_4 = new TP_OrderSummaryListController(tpOrderSummary_3); 

        System.debug('Fetching order explicitly');
        tporderList_4.searchWrapperObj.selectedOppId= null;
       // searchWrapperObj.selectedOppId = null;
        List<Order__c> ordelIst = tporderList_1.fetchOrders('test', 'For_Receiving', null, null, 'Purchase_Order', 'Createddate', 'DESC', null);
        System.debug('ordelIst'+ordelIst);
        
        Test.stopTest(); */  
        }     
    } 

     /*static testMethod void OSC_test2() {
        User uRec = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        
        System.runAs(uRec) {
        
            Id profileId = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'].Id;
            Id roleId = [SELECT Id FROM UserRole WHERE Name =: 'Executive'].Id;
            List<User> userList = new List<User>();
            
            // ======== Create User ======
            for(Integer x=0;x<=5;x++){

                User u = new User(
                    ProfileId = profileId,
                    LastName = 'smpfctestuser'+ String.valueOf(x),
                    Email = 'smpfcTestUser'+ String.valueOf(x) +'@smpfc.com',
                    Username = 'smpfcTestUser@smpfc.com' + String.valueOf(x) + System.currentTimeMillis(),
                    CompanyName = 'SMPFC',
                    Title = 'testTitle',
                    Alias = 'smcU',
                    TimeZoneSidKey = 'Asia/Manila',
                    EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US',
                    isActive = true,
                    UserRoleId = roleId,
                    EmployeeNumber = 'EMP :' + String.valueOf(x)
                );
                system.Debug('**u:'+u);
                system.Debug('**x:'+x);
                userList.add(u);
            }
            insert userlist;

            //========= CREATE TRIGGER FLAG CONTROL ======
            TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            Account_CreateWarehouseForDistributor__c = true,
            Account_CreatePricingCondition__c = true,
            Account_PricingConditionName__c = 'National List Price',
            OrderItem_PlantDetermination__c = true,
            OrderItem_AllowEditAndDelete__c = true
            );
            
            insert settings;

            //======= CREATE ORDER SUMMARY SETTINGS =======
            OrderSummarySettings__c orderStng = new OrderSummarySettings__c(name='Received',OrderStatusColumnsFieldSet__c='Received',OpportunityFieldSetName__c='orderDetail_For_Receiving',OLIFieldSetName__c='OrderItems_For_Receiving' , Page_Size__c = 10);
            insert orderStng;

            //======= CREATE LOADING GROUP =======

            Loading_Group__c newLoadGroup = new Loading_Group__c();
            newLoadGroup.Name = 'Premix';
            newLoadGroup.Grouping__c = 'BMC Dry';
            insert newLoadGroup;

            Loading_Group__c newLoadGroup2 = new Loading_Group__c();
            newLoadGroup2.Name = 'MAGNOLIA Milk';
            newLoadGroup2.Grouping__c = 'BMC Wet';
            insert newLoadGroup2;

            //Create Business Unit
            Market__c newMarket = new Market__c();
            newMarket.Name = 'FEEDS';
            newMarket.Active__c = TRUE;
            newMarket.Market_Code__c = '11C';
            insert newMarket;

            Market__c newMarket2 = new Market__c();
            newMarket2.Name = 'BRANDED';
            newMarket2.Active__c = TRUE;
            newMarket2.Market_Code__c = '12C';
            insert newMarket2;


            TestDataFactory tdf = new TestDataFactory();
            tdf.createCustomSettings();

            List<Account> accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
            List<Account> accChildList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
            List<Contact> newCont = tdf.createContact(5,accList[0].id,'SAS',userList );

            List<Ship_Sold_To__c> shspList = tdf.shpSoldToList(accList, TRUE, accChildList[0]);
            List<Custom_Product__c> prodList =  tdf.createTestProducts(5, NULL, 'Premix', newMarket.id,'');
            List<Custom_Product__c> prodList2 = new List <Custom_Product__c>();

            Custom_Product__c cutProd = new Custom_Product__c();
                cutProd.Name = 'Test Custom Product ' + '-' + System.today() + System.currentTimeMillis();
                cutProd.Active__c = true;
                cutProd.Feeds__c = true;
                cutProd.GFS__c = true;
                cutProd.Poultry__c = true;
                cutProd.Branded__c = true;
                cutProd.Loading_Group_Description__c = 'MAGNOLIA Milk' ;
                cutProd.Material_Type__c = 'Material type';
                cutProd.Market__c = newMarket2.id;
                cutProd.SKU_Code__c = '000009876';
                prodList2.add(cutProd);
            
            system.debug('<<prodList2>>' + prodList);
            system.debug('<<prodList>>' + prodList);
            insert prodList2;     
     
            List<Order__c> orderList = tdf.createTestOrder(1,accList[0].id, newCont[0].id, shspList[0].id, 'Purchase Order','poultry');
            List<Order_Item__c> ordItemList = tdf.createTestOrderItem(prodList,orderList[0].id, 'Purchase Order');
            List<Order_Item__c> ordUpdateList = new List <Order_Item__c>();
        Test.startTest();     
            Id oppId = orderList[0].id;
            Order__c ord = orderList[0];

            TP_OrderSummaryController tpOrderSummary = new TP_OrderSummaryController();
                tpOrderSummary.renderingPanelName = '';
                tpOrderSummary.orderPOId = NULL;

                tpOrderSummary.performSearch();
                tpOrderSummary.renderAsMethod();
                tpOrderSummary.activeTab_Submit();
                tpOrderSummary.backToListView();
                tpOrderSummary.showOpportunityDetails();
                tpOrderSummary.selectedOrderStat = 'Canceled';
                tpOrderSummary.putRenderingPanelName();
            
            SearchWrapper srchwrp = new SearchWrapper(oppId,System.today(),System.today(),'Createddate','DESC');
            TP_OrderSummaryListController tporderList = new TP_OrderSummaryListController(tpOrderSummary);
                Integer tolsize = tporderList.getTotal_size();
                Integer tolPgNmbr = tporderList.getPageNumber();
                Integer limitValue;

                Boolean checkLimit;
                String sortBy = 'DESC';
                String oppRecId;
                String parameterDetails = 'test';
                Integer tolPg = tporderList.getTotalPages();
                Boolean printMode = TRUE;
                srchwrp.printPDFmode = TRUE;
                srchwrp.searchStr = 'test';
                srchwrp.sortedField = NULL;
                srchwrp.sortedOrder = 'DESC';
                srchwrp.offsetValue = NULL;
                srchwrp.stDate = system.today() - 365;
                srchwrp.selectedOppId = oppId;
                srchwrp.offsetValue = NULL;
                srchwrp.totalRowSize = 10;
                srchwrp.pageSize = 2;
                srchwrp.counter = 1;
                srchwrp.enDate = system.today() + 365;
                tporderList.wrapperFromReqParam = srchwrp;
                tporderList.Beginning();
                tporderList.Next();
                tporderList.Previous();
                tporderList.PrintMode = true;
                tporderList.End();
                tporderList.getDisabledPrevious();
                tporderList.getDisabledNext();
                srchwrp.totalRowSize = 2000;
                srchwrp.counter = 0;
                srchwrp.pageSize = 100;
                // tporderList.wrapperFromReqParam = NULL;
                // tporderList.reloadPage();
                tporderList.Previous();
                tporderList.getDisabledNext();

               
                
            TP_OrderSummaryController tpOrderSummaryB = new TP_OrderSummaryController();
                tpOrderSummaryB.searchText = '*';
                tpOrderSummaryB.performSearch();

            

             String brand = cutProd.Brand__c;
             String scale = cutprod.Scale__c;
             String prodGroup =  cutProd.Product_Group__c;
             String prodName = cutProd.Name;
             String matCode = cutProd.SKU_Code__c;
             String itemNumber =  cutProd.SKU_Code__c;
             String uom =  cutProd.Base_UOM__c;
             Decimal decAmt;
             Integer qty;

           // SearchWrapper srchwrp = new SearchWrapper(oppId,System.today(),System.today(),'Createddate','DESC');
            OrderDetailandOrderItemsList.oliProdWrapper testWrap = new OrderDetailandOrderItemsList.oliProdWrapper();

            testWrap.brand = brand;
            testWrap.scale = scale;
            testWrap.prodgroup = prodgroup;
            testWrap.prodName = prodName;
            testWrap.matCode = matCode;
            testWrap.itemNumber = itemNumber;
            testWrap.uom = uom; 
            testWrap.qty = qty;
            testWrap.amt =decAmt;

            OrderDetailandOrderItemsList orderdet = new OrderDetailandOrderItemsList();
            List<String> fldApiNms = new List<String>();
            List<String> fldApiPoultNms = new List<String>();

            orderdet.fldApiNms = fldApiNms;
            orderdet.fldApiPoultNms=fldApiPoultNms;
            orderdet.alertClass = '';
            orderdet.alertMessage='';
            orderdet.acclist = accList;
            orderdet.isPoultry = FALSE;
            
            srchwrp.selectedOppId = oppId;  
            srchwrp.oliCsDet = OrderSummarySettings__c.getInstance('Received');
            orderdet.SearchWrapperCls = srchwrp;
            Order__c temOrder = orderdet.opp;
            String stageName = orderdet.stageName;
    
            orderdet.SaveOppLineItems();
          
        Test.stopTest();   
        }     
    }
    static testMethod void OSC_test3() {
         Id profileId = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'].Id;
         Id roleId = [SELECT Id FROM UserRole WHERE Name =: 'Executive'].Id;
         
            List<User> userList = new List<User>();
            
            // ======== Create User ======
            for(Integer x=0;x<=5;x++){

                User u = new User(
                    ProfileId = profileId,
                    LastName = 'smpfctestuser'+ String.valueOf(x),
                    Email = 'smpfcTestUser'+ String.valueOf(x) +'@smpfc.com',
                    Username = 'smpfcTestUser@smpfc.com' + String.valueOf(x) + System.currentTimeMillis(),
                    CompanyName = 'SMPFC',
                    Title = 'testTitle',
                    Alias = 'smcU',
                    TimeZoneSidKey = 'Asia/Manila',
                    EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US',
                    isActive = true,
                    UserRoleId = roleId,
                    EmployeeNumber = 'EMP :' + String.valueOf(x)
                );
                system.Debug('**u:'+u);
                system.Debug('**x:'+x);
                userList.add(u);
            }
            insert userlist;
             User uRec = [SELECT Id FROM User WHERE ProfileId =:profileId  AND IsActive =: TRUE LIMIT 1];

            //========= CREATE TRIGGER FLAG CONTROL ======
            TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            Account_CreateWarehouseForDistributor__c = true,
            Account_CreatePricingCondition__c = true,
            Account_PricingConditionName__c = 'National List Price',
            OrderItem_PlantDetermination__c = true,
            OrderItem_AllowEditAndDelete__c = true
            );
            
            insert settings;

            //======= CREATE ORDER SUMMARY SETTINGS =======
            OrderSummarySettings__c orderStng = new OrderSummarySettings__c(name='Received',OrderStatusColumnsFieldSet__c='Received',OpportunityFieldSetName__c='orderDetail_For_Receiving',OLIFieldSetName__c='OrderItems_For_Receiving' , Page_Size__c = 10);
            insert orderStng;

            //======= CREATE LOADING GROUP =======

            Loading_Group__c newLoadGroup = new Loading_Group__c();
            newLoadGroup.Name = 'Premix';
            newLoadGroup.Grouping__c = 'BMC Dry';
            insert newLoadGroup;

            Loading_Group__c newLoadGroup2 = new Loading_Group__c();
            newLoadGroup2.Name = 'MAGNOLIA Milk';
            newLoadGroup2.Grouping__c = 'BMC Wet';
            insert newLoadGroup2;

            //Create Business Unit
            Market__c newMarket = new Market__c();
            newMarket.Name = 'FEEDS';
            newMarket.Active__c = TRUE;
            newMarket.Market_Code__c = '11C';
            insert newMarket;

            Market__c newMarket2 = new Market__c();
            newMarket2.Name = 'BRANDED';
            newMarket2.Active__c = TRUE;
            newMarket2.Market_Code__c = '12C';
            insert newMarket2;


            TestDataFactory tdf = new TestDataFactory();
            tdf.createCustomSettings();

            List<Account> accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
            List<Account> accChildList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
            List<Contact> newCont = tdf.createContact(5,accList[0].id,'SAS',userList );

            List<Ship_Sold_To__c> shspList = tdf.shpSoldToList(accList, TRUE, accChildList[0]);
            List<Custom_Product__c> prodList =  tdf.createTestProducts(5, NULL, 'Premix', newMarket.id, 'SWINE BLOOD');
            List<Custom_Product__c> prodList2 = new List <Custom_Product__c>();

            Custom_Product__c cutProd = new Custom_Product__c();
                cutProd.Name = 'Test Custom Product ' + '-' + System.today() + System.currentTimeMillis();
                cutProd.Active__c = true;
                cutProd.Feeds__c = true;
                cutProd.GFS__c = true;
                cutProd.Poultry__c = true;
                cutProd.Branded__c = true;
                cutProd.Loading_Group_Description__c = 'MAGNOLIA Milk' ;
                cutProd.Material_Type__c = 'Material type';
                cutProd.Market__c = newMarket2.id;
                cutProd.SKU_Code__c = '000009876';
                prodList2.add(cutProd);
            
            system.debug('<<prodList2>>' + prodList);
            system.debug('<<prodList>>' + prodList);
            insert prodList2;     
            
            List<Order__c> orderList = tdf.createTestOrder(1,accList[0].id); //tdf.createTestOrder(1,accList[0].id, newCont[0].id, shspList[0].id, 'Purchase Order','12GO');
            List<Order_Item__c> ordItemList = tdf.createTestOrderItem(prodList,orderList[0].id, 'Purchase Order');

            List<Order_Item__c> ordUpdateList = new List <Order_Item__c>();
         
            TP_OrderSummaryController tpOrderSummary = new TP_OrderSummaryController();
                tpOrderSummary.performSearch();
                tpOrderSummary.renderAsMethod();
                tpOrderSummary.activeTab_Submit();
                tpOrderSummary.backToListView();
                tpOrderSummary.showOpportunityDetails();
                tpOrderSummary.selectedOrderStat = 'Canceled';
 
       Test.startTest();        
            TP_OrderSummaryListController tporderList = new TP_OrderSummaryListController(tpOrderSummary);
                tporderList.reloadPage();
                tporderList.doSorting();
                tporderList.Beginning();
                tporderList.Next();
                tporderList.Previous();
                tporderList.printMode = TRUE;
                tporderList.reloadPage();
                tporderList.End();
                tporderList.getDisabledPrevious();
                tporderList.getDisabledNext();
                Integer tolsize = tporderList.getTotal_size();
                Integer tolPgNmbr = tporderList.getPageNumber();
                Integer limitValue;
                Boolean checkLimit;
                String sortBy = 'desc';
                Integer tolPg = tporderList.getTotalPages();

                
            TP_OrderSummaryController tpOrderSummaryB = new TP_OrderSummaryController();
                tpOrderSummaryB.searchText = '*';
                tpOrderSummaryB.performSearch();

            Id oppId = orderList[0].id;
            Order__c ord = orderList[0];

             String brand = cutProd.Brand__c;
             String scale = cutprod.Scale__c;
             String prodGroup =  cutProd.Product_Group__c;
             String prodName = cutProd.Name;
             String matCode = cutProd.SKU_Code__c;
             String itemNumber =  cutProd.SKU_Code__c;
             String uom =  cutProd.Base_UOM__c;
             Decimal decAmt;
             Integer qty;

            SearchWrapper srchwrp = new SearchWrapper(oppId,System.today(),System.today(),'Createddate','DESC');
            OrderDetailandOrderItemsList.oliProdWrapper testWrap = new OrderDetailandOrderItemsList.oliProdWrapper();
            testWrap.brand = brand;
            testWrap.scale = scale;
            testWrap.prodgroup = prodgroup;
            testWrap.prodName = prodName;
            testWrap.matCode = matCode;
            testWrap.itemNumber = itemNumber;
            testWrap.uom = uom; 
            testWrap.qty = qty;
            testWrap.amt =decAmt;

            OrderDetailandOrderItemsList orderdet = new OrderDetailandOrderItemsList();
            List<String> fldApiNms = new List<String>();
            List<String> fldApiPoultNms = new List<String>();

            orderdet.fldApiNms = fldApiNms;
            orderdet.fldApiPoultNms=fldApiPoultNms;
            orderdet.alertClass = '';
            orderdet.alertMessage='';
            orderdet.acclist = accList;
            orderdet.isPoultry = FALSE;
            
            srchwrp.selectedOppId = oppId;  
            srchwrp.oliCsDet = OrderSummarySettings__c.getInstance('Received');
            orderdet.SearchWrapperCls = srchwrp;
            Order__c temOrder = orderdet.opp;
            String stageName = orderdet.stageName;
    
            orderdet.SaveOppLineItems();
          
        Test.stopTest();   
           
    }*/
 
}
@isTest
public class FetchOrderRecordsTest {



    public static void createCustomSettings() {

        List<RDDCutOff__c> rddCutOffList = new List<RDDCutOff__c>();

        RDDCutOff__c rddCutOff0 = new RDDCutOff__c(
            Name = 'Branded',
            Days__c = 2
        );

        RDDCutOff__c rddCutOff1 = new RDDCutOff__c(
            Name = 'POULTRY',
            Days__c = 2,
            Time__c = '13:00'
        );

        rddCutOffList.add(rddCutOff0);
        rddCutOffList.add(rddCutOff1);

        Insert rddCutOffList;

        List<BU_Mapping__c> buMapping = new List<BU_Mapping__c>();

        BU_Mapping__c brandedMapping = new BU_Mapping__c();

        brandedMapping.Name = 'MAGNOLIA';
        brandedMapping.Account_Field_Name__c = 'Branded__c';

        buMapping.add(brandedMapping);

        BU_Mapping__c poultryMapping = new BU_Mapping__c();

        poultryMapping.Name = 'MEATS';
        poultryMapping.Account_Field_Name__c = 'BU_Poultry__c';

        buMapping.add(poultryMapping);

        BU_Mapping__c gfsMapping = new BU_Mapping__c();

        gfsMapping.Name = 'GFS';
        gfsMapping.Account_Field_Name__c = 'GFS__c';

        buMapping.add(gfsMapping);

        BU_Mapping__c feedsMapping = new BU_Mapping__c();

        feedsMapping.Name = 'FEEDS';
        feedsMapping.Account_Field_Name__c = 'BU_Feeds__c';

        buMapping.add(feedsMapping);

        insert buMapping;

        List<Sales_Org_Division__c> salesOrgDivision = new List<Sales_Org_Division__c>();

        Sales_Org_Division__c brandedSalesOrg = new Sales_Org_Division__c();

        brandedSalesOrg.Name = '14F0-MAGNOLIA-10';
        brandedSalesOrg.Business_Unit__c = 'MAGNOLIA';
        brandedSalesOrg.Code__c = '14F0';
        brandedSalesOrg.Description__c = 'Branded';
        brandedSalesOrg.Division__c = '10';

        salesOrgDivision.add(brandedSalesOrg);

        Sales_Org_Division__c poultrySalesOrg = new Sales_Org_Division__c();

        poultrySalesOrg.Name = '11B0-MEATS-20';
        poultrySalesOrg.Business_Unit__c = 'MEATS';
        poultrySalesOrg.Code__c = '11B0';
        poultrySalesOrg.Description__c = 'Agro/Commodity';
        poultrySalesOrg.Division__c = '20';

        salesOrgDivision.add(poultrySalesOrg);

        Sales_Org_Division__c gfsSalesOrg = new Sales_Org_Division__c();

        gfsSalesOrg.Name = '11D0-GFS-10';
        gfsSalesOrg.Business_Unit__c = 'GFS';
        gfsSalesOrg.Code__c = '11D0';
        gfsSalesOrg.Division__c = '10';

        salesOrgDivision.add(gfsSalesOrg);

        Sales_Org_Division__c gfsSalesOrg2 = new Sales_Org_Division__c();

        gfsSalesOrg2.Name = '11D0-GFS-20';
        gfsSalesOrg2.Business_Unit__c = 'GFS';
        gfsSalesOrg2.Code__c = '11D0';
        gfsSalesOrg2.Division__c = '20';

        salesOrgDivision.add(gfsSalesOrg2);

        Sales_Org_Division__c feedsSalesOrg = new Sales_Org_Division__c();

        feedsSalesOrg.Name = '11C0-FEEDS-20';
        feedsSalesOrg.Business_Unit__c = 'FEEDS';
        feedsSalesOrg.Code__c = '11C0';
        feedsSalesOrg.Description__c = 'Agro/Commodity';
        feedsSalesOrg.Division__c = '20';

        salesOrgDivision.add(feedsSalesOrg);

        insert salesOrgDivision;
    }


   public static Account setupdata(){

        createCustomSettings();


        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'National List Price',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);

        Market__c poultryMarket2 = new Market__c(
            Name = 'POULTRY',
            Active__c = true,
            Market_Code__c = '04'
            );
        marketList.add(poultryMarket2);
        insert marketList;
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            Account_CreateWarehouseForDistributor__c = true,
            Account_CreatePricingCondition__c = true,
            Account_PricingConditionName__c = 'National List Price',
            SIF_AllowEditAndDelete__c = true
            );
        insert settings;
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id distributorCustomerId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Sample Grocery1';
        acc.Sales_Organization__c  = 'Branded';
        acc.General_Trade__c  = true;
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.AccountNumber = '123412344';
        acc.Market__c = marketList.get(0).Id;
        acc.RecordTypeId = distributorAccountId;
        insert acc;
        Account acc2 = new Account();
        acc2.Name = 'Sample Grocery2';
        acc2.Sales_Organization__c  = 'Branded';
        acc2.General_Trade__c  = true;
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.AccountNumber = '1234123';
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = acc.Id;
        acc2.RecordTypeId = distributorCustomerId;
        insert acc2;
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();

         //CREATE SAS- Contact
        Id SASrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Contact SASvar = new Contact(RecordTypeId=SASrecordTypeId, FirstName = 'xyzFirst', LastName = 'XyZLast', AccountId = acc.Id);
        insert SASvar;

        //AARP: Create New DSP
        Id dspRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Contact dspVar = new Contact (RecordTypeId=dspRecordTypeId, FirstName = 'xyzSecond1', LastName = 'XyZSecond1', AccountId = acc.Id);
        insert dspVar;

        Order__c orderRecord = new Order__c(
            Account__c = acc2.Id,
            RecordTypeId = actualSalesRecordTypeId,
            Invoice_Date__c = System.today().addDays(-3),
            Invoice_No__c = '1111111',
            PO_No__c   = 'bar@19526',
            SAS__c = SASvar.Id,
            DSP__c = DSPvar.Id,
            Status__c = 'Submitted'
            );
        insert orderRecord;

        return acc2;

    }


    public static testMethod void testFetchOrderRecordsPositive() {

        Account acc2 = setupdata();

        String json= '['+
                     '  {'+
                     '    "accountId" : "' + acc2.Id + '",'+
                     '    "poNumber"  : "bar@19526"'+
                     '  },'+
                     '  {'+
                     '    "accountId" : "0012800001V6Zcq",'+
                     '    "poNumber"  : "bar@19527"'+
                     '  }'+
                     ']';

        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchOrderRecords';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            List<WebServiceResponse.OrderResponse> resultRes = FetchOrderRecords.doPost();
        System.Test.stopTest();


        System.assertEquals('bar@19526', resultRes.get(0).poNumber);




    }

    public static testMethod void testFetchOrderRecordsNegetive() {

        Account acc2 = setupdata();

        String json= '['+
                     '  {'+
                     '    "accountId" : "0012800001V6Zcq",'+
                     '    "poNumber"  : "bar@19526"'+
                     '  },'+
                     '  {'+
                     '    "accountId" : "0012800001V6Zcq",'+
                     '    "poNumber"  : "bar@19527"'+
                     '  }'+
                     ']';

        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchOrderRecords';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
             List<WebServiceResponse.OrderResponse> resultRes = FetchOrderRecords.doPost();
        System.Test.stopTest();


        System.debug('--se--' + resultRes.get(0).errors);
        System.assertEquals(false, resultRes.get(0).errors.isEmpty());


    }

    public static testMethod void testFetchOrderRecordsJSONFail() {

        Account acc2 = setupdata();

        String json= '['+
                     '  {'+
                     '    "accountId" "": "0012800001V6Zcq",'+
                     '    "poNumber"  : "bar@19526"'+
                     '  },'+
                     '  {'+
                     '    "accountId" : "0012800001V6Zcq",'+
                     '    "poNumber"  : "bar@19527"'+
                     '  }'+
                     ']';

        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchOrderRecords';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            List<WebServiceResponse.OrderResponse> resultRes = FetchOrderRecords.doPost();
        System.Test.stopTest();


        System.debug('--se--' + resultRes.get(0).errors);

        System.assertEquals(false, resultRes.get(0).errors.isEmpty());




    }

    public static testMethod void testWebService() {

        WebServiceResponse.OrderRequest reqObj = new WebServiceResponse.OrderRequest('Test', 'Sample');
        WebServiceResponse.OrderResponse reqObj2 = new WebServiceResponse.OrderResponse( '0012800001MoMcm'
                                                                               , new List<Id> {'0012800001MoMcm'}
                                                                               , true
                                                                               , new List<String> {'errors'}
                                                                               , 'sfoId'
                                                                               , 'dateToday'
                                                                               , 'lastModifiedDate'
                                                                               , 'status'
                                                                              );

        WebServiceResponse.OrderResponse reqObj3 = new WebServiceResponse.OrderResponse( 'accountId'
                                                                               , 'poNumber'
                                                                               );

        WebServiceResponse.OrderItemResponse reqObj4 = new WebServiceResponse.OrderItemResponse( new List<Id> {'0012800001MoMcm'}
                                                                                               , true
                                                                                               , new List<String> {'errors'}
                                                                                               );

    }

}
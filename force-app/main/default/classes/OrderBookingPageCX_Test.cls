@isTest
private class OrderBookingPageCX_Test{
        
    Static List<User> uRec;
    Static List<User> uRec1;
    Static List<Account> accAdminList; 
    Static List<Account> accList;
    Static List<AccountContactRelation> acrList;
    Static List<Category__c> catList;
    Static List<Custom_Product__c> prodList;
        
    static void init(){
                
        String sasRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();  
        String disRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();      
                
        //Insert UOM
        UOM__c uom = new UOM__c(
        Unique_UOM__c = 'KG',
        Name = 'KG',
        SAP_Code__c = 'KG'
        );
        Insert uom;
    
        //Insert Category
        catList = new List<Category__c>();
        Category__c cat = new Category__c(
        Category_Code__c = '0001',
        Dataloader_ID__c = '0001',
        EIS_Category__c = 'Pork',
        Name = 'PORK',
        SAP_Code__c = '0001',
        Business_Unit__c = 'GFS; Poultry',
        UOM__c = uom.Id
        );
        
        Category__c cat2 = new Category__c(
        Category_Code__c = '0002',
        Dataloader_ID__c = '0002',
        EIS_Category__c = 'Beef',
        Name = 'BEEF',
        SAP_Code__c = '0002',
        Business_Unit__c = 'GFS; Poultry',
        UOM__c = uom.Id
        );
        
        Category__c cat3 = new Category__c(
        Category_Code__c = '0003',
        Dataloader_ID__c = '0003',
        EIS_Category__c = 'BMC JMSO',
        Name = 'BMC JMSO',
        SAP_Code__c = '0003',
        Business_Unit__c = 'Branded; GFS',
        UOM__c = uom.Id
        );
        
        Category__c cat4 = new Category__c(
        Category_Code__c = '0004',
        Dataloader_ID__c = '0004',
        EIS_Category__c = 'CHICKEN',
        Name = 'CHICKEN',
        SAP_Code__c = '0004',
        Business_Unit__c = 'GFS; Poultry',
        UOM__c = uom.Id
        );
        
        catList.add(cat);
        catList.add(cat2);
        catList.add(cat3);
        catList.add(cat4);
        Insert catList;
        
        Category_Combination__c catComb1 = new Category_Combination__c(
        Name = 'BMC JMSO',
        Child_Categories__c = String.valueOf(cat2.Id).left(15)+'~'+String.valueOf(cat.Id).left(15)+'~'+String.valueOf(cat4.Id).left(15),
        Parent_Category__c = String.valueOf(cat3.Id).left(15)
        );

        Insert catComb1;
        
        //INSERT CHAIN
        List<Chains__c> chList = new List<Chains__c>();
        Chains__c ch0 = new Chains__c(
        Group_No__c = 0,
        Description__c = 'Wet Market Deal'
        );
        
        Chains__c ch1 = new Chains__c(
        Group_No__c = 0,
        Description__c = 'Market'
        );
        
        chList.add(ch0);
        chList.add(ch1);
        Insert chList;
    
        uRec = [SELECT Id FROM User WHERE Branded__c =: FALSE AND Poultry__c =: TRUE AND Profile.Name = 'Sales User' AND IsActive =: TRUE LIMIT 3];
        uRec1 = [SELECT Id FROM User WHERE Branded__c =: FALSE AND Poultry__c =: TRUE AND Profile.Name = 'Distributor User' AND IsActive =: TRUE LIMIT 1];
        uRec[0].EmployeeNumber = '0000000';
        uRec[1].EmployeeNumber = '0000001';
        uRec[2].EmployeeNumber = '0000002';
        uRec1[0].EmployeeNumber = '0000003';
        Update uRec;
        Update uRec1;
        
        //INSERT CUSTOM SETTINGS
        TestDataFactory tdf = new TestDataFactory();
        tdf.createCustomSettings();
        /*List<Custom_Product__c>*/ prodList =  tdf.createTestProducts(40);
        accAdminList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
        accList = tdf.createTestAccount(3, True, True, 'Distributor/ Direct',null);
        
        List<Account> accShipToList0 = new List<Account>();
        accShipToList0.add(accList[0]);
        accShipToList0.add(accList[1]);
        List<Account> accShipToList1 = new List<Account>();
        accShipToList1.add(accList[2]);
        
        List<Ship_Sold_To__c> shspList = tdf.shpSoldToList(accShipToList0, FALSE, NULL);
        List<Ship_Sold_To__c> shspList1 = new List<Ship_Sold_To__c>();
        
        Ship_Sold_To__c spTo0 = new Ship_Sold_To__c(
            AccountMaster__c = accList[0].Id,
            AccountChild__c = accList[1].Id,
            Ship_Sold_To__c = 'SH',
            Dataloader_ID__c = accList[1].Id+'SP2'
        );
        
        Ship_Sold_To__c spTo1 = new Ship_Sold_To__c(
            AccountMaster__c = accList[0].Id,
            AccountChild__c = accList[2].Id,
            Ship_Sold_To__c = 'SH',
            Dataloader_ID__c = accList[2].Id+'SP3'
        );
        
        Ship_Sold_To__c spTo2 = new Ship_Sold_To__c(
            AccountMaster__c = accList[2].Id,
            AccountChild__c = accList[0].Id,
            Ship_Sold_To__c = 'SP',
            Dataloader_ID__c = accList[2].Id+'SP2'
        );
        
        shspList1.add(spTo0);
        shspList1.add(spTo1); 
        shspList1.add(spTo2); 
        Insert shspList1;
        
        accList[0].CustomerGroup2__c = ch0.Id;
        accList[1].CustomerGroup2__c = ch1.Id;
        Update accList;

        //ACCOUNT TEAM
        List<AccountTeamMember> atList = new List<AccountTeamMember>();
        AccountTeamMember at0 = new AccountTeamMember(
        AccountId = accList[0].id,
        UserId = uRec[0].Id,
        TeamMemberRole = 'Account Modifier'
        );
        
        AccountTeamMember at1 = new AccountTeamMember(
        AccountId = accList[1].id,
        UserId = uRec[0].Id,
        TeamMemberRole = 'Account Modifier'
        );
        
        AccountTeamMember at2 = new AccountTeamMember(
        AccountId = accList[2].id,
        UserId = uRec[0].Id,
        TeamMemberRole = 'Account Modifier'
        );
        
        AccountTeamMember at3 = new AccountTeamMember(
        AccountId = accList[0].id,
        UserId = uRec1[0].Id,
        TeamMemberRole = 'Account Modifier'
        );
        
        atList.add(at0); 
        atList.add(at1); 
        atList.add(at2);
        atList.add(at3);
        Insert atList;

        Account acc0 = [SELECT Id, Customer_Group_2_Code__c FROM ACCOUNT WHERE Id =: accList[0].Id];
        
        //INSERT CONTACT
        List<Contact> conList = new List<Contact>();
        Contact con0 = new Contact(
        FirstName = 'First0',
        LastName = 'Last0',
        AccountId = acc0.id,
        Is_Primary__c = TRUE,
        Employee_Number__c = '0000000',
        RecordTypeId = sasRecordTypeId);
        
        Contact con1 = new Contact(
        FirstName = 'First1',
        LastName = 'Last1',
        AccountId = acc0.id,
        Employee_Number__c = '0000001',
        RecordTypeId = sasRecordTypeId);
        
        Contact condis = new Contact(
        FirstName = 'FirstDis',
        LastName = 'Lastdis',
        AccountId = accList[0].id,
        Is_Primary__c = TRUE,
        Email = 'dis@test.com',
        Employee_Number__c = '0000003',
        RecordTypeId = disRecordTypeId);

        conList.add(con0);
        conList.add(con1);
        conList.add(condis);
        Insert conList;
        
        //INSERT ACCOUNT CONTACT RELATION
        acrList = new List<AccountContactRelation>();
        AccountContactRelation acr = new AccountContactRelation(
        AccountId = accList[1].Id,
        ContactId = con0.Id
        );
        
        AccountContactRelation acr1 = new AccountContactRelation(
        AccountId = accList[2].Id,
        ContactId = con0.Id
        );
        
        AccountContactRelation acr2 = new AccountContactRelation(
        AccountId = accList[2].Id,
        ContactId = con1.Id
        );
        
        acrList.add(acr);
        acrList.add(acr1);
        acrList.add(acr2);
        Insert acrList; 
    }    
        
    public static testmethod void OrderBookingPage_Poultry() {
        
        init();
        
        Test.startTest();

            System.runAs(uRec[0]) {
                    
                Order__c ordRec = new Order__c();  
                
                Test.setCurrentPageReference(new PageReference('Page.OrderBookingPageCX')); 
                System.currentPageReference().getParameters().put('var1', '0012800000GqSXn'); 
                ApexPages.StandardController con = new ApexPages.StandardController(ordRec);
                OrderBookingPageCX cx = new OrderBookingPageCX(con);
                cx.businessUnitValue = 'POULTRY';

                List<AccountContactRelation> acrCheckList0 = [SELECT Id, AccountId, ContactId, Contact.Employee_Number__c, Contact.Contact_Full_Name__c, Account.Name FROM AccountContactRelation WHERE Id =: acrList[2].Id];
                cx.populateSASDSP(acrCheckList0);
                
                List<AccountContactRelation> acrCheckList1 = [SELECT Id, AccountId, ContactId, Contact.Employee_Number__c, Contact.Contact_Full_Name__c, Account.Name FROM AccountContactRelation WHERE Id =: acrList[1].Id];
                cx.populateSASDSP(acrCheckList1);
                
                cx.siw.o.Account__c = accAdminList[0].Id; //EXCEPTION RECORD
                cx.refreshRelatedFields();
                cx.refreshSASRelatedFields();
                
                cx.siw.o.Account__c = accList[1].Id;
                cx.refreshRelatedFields();
                cx.refreshSASRelatedFields();
                
                cx.siw.o.Account__c = accList[2].Id;               
                cx.refreshRelatedFields();
                cx.refreshSASRelatedFields();
                
                cx.businessUnitValue = 'FEEDS';
                cx.populateSASDSP();
                
                cx.businessUnitValue = 'MEATS';
                cx.populateSASDSP();
                
                cx.businessUnitValue = 'BRANDED';
                cx.populateSASDSP();
                
                cx.businessUnitValue = 'POULTRY';
                cx.populateSASDSP();
                
                cx.btnAccountSearch();
            }              
    }
    
    public static testmethod void OrderBookingPage_Distributor() {
        
        init();
  
        Test.startTest(); 
            System.runAs(uRec1[0]) {
            
                Order__c ordRec1 = new Order__c ();  
                
                Test.setCurrentPageReference(new PageReference('Page.OrderBookingPageCX')); 
                System.currentPageReference().getParameters().put('var1', NULL);    
                ApexPages.StandardController sc1 = new ApexPages.StandardController(ordRec1);
                OrderBookingPageCX cx1 = new OrderBookingPageCX(sc1);
                cx1.businessUnitValue = 'POULTRY';
                cx1.siw.o.Product_Category__c = catList[0].Id;
                cx1.setSASDistributor();
                
                cx1.siw.o.Product_Category__c = catList[3].Id;
                cx1.businessUnitValue = 'POULTRY';
                cx1.setSASDistributor();
                cx1.productSearchKey = 'Test Custom Product';
                cx1.siw.o.Product_Category__c = prodList[0].Category1__c;
                cx1.searchProducts();
                cx1.btnsetDraft();
                
                cx1.btnAccountSearch();
                cx1.valitdateProductQty();
                cx1.btnBack();
                cx1.backRecord();
                
            }
        Test.stopTest(); 
    } 
    
    public static testmethod void OrderBookingPage_searchProducts() {
        
        init();
        
        prodList[0].WetMarket_Shortlist__c  = TRUE;
        prodList[0].Category1__c = catList[3].Id;
        
        prodList[3].WetMarket_Shortlist__c  = TRUE;
        prodList[3].Category1__c = catList[3].Id;
        
        prodList[4].WetMarket_Shortlist__c  = TRUE;
        prodList[4].Category1__c = catList[3].Id;
        
        prodList[5].WetMarket_Shortlist__c  = TRUE;
        prodList[5].Category1__c = catList[3].Id;
                
        update prodList;
        
        Pricing_Condition__c gmaPricingCondition = new Pricing_Condition__c(name='test');
        insert gmaPricingCondition;
        
        Id listCustSpecPriceDatype = Schema.SObjectType.Pricing_Condition_Data__c.getRecordTypeInfosByName().get('Customer Specific Price').getRecordTypeId();
        Id listPriceDatype = Schema.SObjectType.Pricing_Condition_Data__c.getRecordTypeInfosByName().get('List Price').getRecordTypeId();
        List<Pricing_Condition_Data__c> gmaPricigConditionDataList = new List<Pricing_Condition_Data__c>();
        Pricing_Condition_Data__c tempPricingConditionData = new Pricing_Condition_Data__c();
             
        for(Custom_Product__c cp :prodList){
            tempPricingConditionData = new Pricing_Condition_Data__c();
            tempPricingConditionData.Pricing_Condition__c = gmaPricingCondition.id;
            tempPricingConditionData.Product__c = cp.id;
            tempPricingConditionData.RecordTypeId = listPriceDatype;
            gmaPricigConditionDataList.add(tempPricingConditionData);
        }
        
        gmaPricigConditionDataList[3].RecordTypeId = listCustSpecPriceDatype;
        gmaPricigConditionDataList[4].RecordTypeId = listCustSpecPriceDatype;
        gmaPricigConditionDataList[5].RecordTypeId = listCustSpecPriceDatype;
        
        Insert gmaPricigConditionDataList;
          
        Test.startTest();             
            
            Order__c ordRec1 = new Order__c ();
            Test.setCurrentPageReference(new PageReference('Page.OrderBookingPageCX')); 
            System.currentPageReference().getParameters().put('var1', accList[1].Id);
            ApexPages.StandardController sc1 = new ApexPages.StandardController(ordRec1);
            OrderBookingPageCX cx1 = new OrderBookingPageCX(sc1);
            
            //WITHOUT PROD CAT
            cx1.siw.o.Account__c = accList[0].Id;
            cx1.isNextPrev = TRUE;
            cx1.searchProducts();
            
            //WITH PROD CAT COMBI
            cx1.siw.o.Product_Category__c = catList[2].Id;
            cx1.siw.o.Account__c = accList[0].Id;
            cx1.isNextPrev = FALSE;
            cx1.searchNonShortListProducts();
            
            //WITH PROD CAT
            cx1.siw.o.Product_Category__c = catList[3].Id;
            cx1.siw.o.Account__c = accList[0].Id;
            cx1.isShortList = TRUE;
            cx1.isNextPrev = FALSE;
            cx1.searchViewFlagger = 0;
            cx1.searchShortListProducts();
            
            //WITH PROD CAT COMBI
            cx1.siw.o.Product_Category__c = catList[2].Id;
            cx1.siw.o.Account__c = accList[0].Id;
            cx1.isInitialLoad = TRUE;
            cx1.isNextPrev = FALSE;
            cx1.productSearchKey = 'Test';
            cx1.searchNonShortListProducts();
            
            //WITH PROD CAT
            cx1.siw.o.Product_Category__c = catList[3].Id;
            cx1.siw.o.Account__c = accList[0].Id;
            cx1.isInitialLoad = TRUE;
            cx1.isNextPrev = FALSE;
            cx1.searchShortListProducts();
            
            cx1.getSelectedOliwFromTemporaryStorage();
            
            cx1.validateFields();
            
            cx1.siw.o.PO_No__c = 'TESTOBPCX';
            cx1.validateFields();
            
            cx1.siw.o.PO_No__c = 'TESTOBPCX';
            cx1.siw.o.Delivery_Time_From__c = '1/1/2099';
            cx1.validateFields();
            
            cx1.siw.o.PO_No__c = 'TESTOBPCX';
            cx1.u.Branded__c = FALSE;
            cx1.validateFields();
            
            cx1.siw.o.PO_No__c = 'TESTOBPCX';
            cx1.u.Branded__c = FALSE;
            cx1.siw.rddStr = '2018-10-20';
            cx1.validateFields();
            
            cx1.siw.o.PO_No__c = 'TESTOBPCX';
            cx1.u.Branded__c = FALSE;
            cx1.siw.rddStr = NULL;
            cx1.validateFields();
            
            cx1.siw.o.PO_No__c = 'TESTOBPCX';
            cx1.siw.podateStr = NULL;
            cx1.validateFields();
            
            cx1.siw.o.PO_No__c = 'TESTOBPCX';
            cx1.siw.podateStr = '2018-10-20';
            cx1.validateFields();            
            
            cx1.getNationalPricing();
            
            cx1.btnAccountSearch();
            
            cx1.searchProductsRefresh();
            
            cx1.businessUnitValue = 'branded';
            cx1.searchProductsRefresh();
            
        Test.stopTest(); 
    }
    
    static testMethod void OrderBookingPage_Branded_A() {

        TestDataFactory objTestDataFactory = new TestDataFactory();

        List<Custom_Product__c> prodList =  objTestDataFactory.createTestUOMProducts(20, TRUE, FALSE);
        
        List<Account> DistributorAccobj = TestDataFactory.createAccounts(1,'Distributor/ Direct',true);
        
        Ship_Sold_To__c spTo = new Ship_Sold_To__c(
            AccountMaster__c = DistributorAccobj[0].Id,
            AccountChild__c = DistributorAccobj[0].Id,
            Ship_Sold_To__c = 'SP',
            Dataloader_ID__c = DistributorAccobj[0].Id+'SP2'
        );
        Insert spTo;
        
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
        OrderItem_PlantDetermination__c = TRUE,
        orderItemOldTrigger__c = FALSE
        );
        insert settings;
        
        Validation_Rules__c validationRule = new Validation_Rules__c(
        Order_RequestedDeliveryDateLessPODate__c = true
        );
        insert validationRule;
        
        List<Order__c > orderList = objTestDataFactory.createTestOrder(10,DistributorAccobj[0].id);
        orderList[0].Ship_Sold_To__c = spTo.Id;
        orderList[0].requested_Delivery_Date__c = system.today()+2;
        orderList[0].Invoice_Date__c = system.today()+3;
        orderList[0].PO_Date__c = system.today()+3;
        orderList[1].Ship_Sold_To__c = NULL;
        orderList[1].requested_Delivery_Date__c = system.today()+2;
        orderList[1].Invoice_Date__c = system.today()+3;
        orderList[1].PO_Date__c = system.today()+3;
        update orderList;
        
        List<Order_Item__c> orderItemList = objTestDataFactory.createTestUOMOrderItem(prodList,orderList[0].id, 'CS');
        for(Order_Item__c objorderItem : orderItemList){
            objorderItem.Estimated_Amount__c = 10;
            objorderItem.Order_Quantity__c = 1;
        }
        update orderItemList; 
                
        Order__c ordRec1 = new Order__c ();
        Test.setCurrentPageReference(new PageReference('Page.OrderBookingPageCX')); 
        System.currentPageReference().getParameters().put('order_id', orderList[0].Id);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(ordRec1);
        OrderBookingPageCX cx1 = new OrderBookingPageCX(sc1);
        cx1.isInitialLoad = TRUE;
        cx1.getnxt();
        cx1.getprev();
        cx1.next();
        //cx1.previous();
        cx1.accountNumberMap.put(orderList[0].Account__c, orderList[0].Account__c);
        cx1.RDDDate = orderList[0].requested_Delivery_Date__c+1;
        cx1.validateOrder(orderList[0], prodList);

    }
    
    static testMethod void OrderBookingPage_Branded_B() {

        TestDataFactory objTestDataFactory = new TestDataFactory();

        List<Custom_Product__c> prodList =  objTestDataFactory.createTestUOMProducts(20, TRUE, FALSE);
        
        List<Account> DistributorAccobj = TestDataFactory.createAccounts(1,'Distributor/ Direct',true);
        
        Ship_Sold_To__c spTo = new Ship_Sold_To__c(
            AccountMaster__c = DistributorAccobj[0].Id,
            AccountChild__c = DistributorAccobj[0].Id,
            Ship_Sold_To__c = 'SP',
            Dataloader_ID__c = DistributorAccobj[0].Id+'SP2'
        );
        Insert spTo;
        
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
        OrderItem_PlantDetermination__c = TRUE,
        orderItemOldTrigger__c = FALSE
        );
        insert settings;
        
        Validation_Rules__c validationRule = new Validation_Rules__c(
        Order_RequestedDeliveryDateLessPODate__c = true
        );
        insert validationRule;
        
        List<Order__c > orderList = objTestDataFactory.createTestOrder(10,DistributorAccobj[0].id);
        orderList[0].Ship_Sold_To__c = spTo.Id;
        orderList[0].requested_Delivery_Date__c = system.today()+2;
        orderList[0].Invoice_Date__c = system.today()+3;
        orderList[0].PO_Date__c = system.today()+3;
        orderList[1].Ship_Sold_To__c = NULL;
        orderList[1].requested_Delivery_Date__c = system.today()+2;
        orderList[1].Invoice_Date__c = system.today()+3;
        orderList[1].PO_Date__c = system.today()+3;
        update orderList;
        
        List<Order_Item__c> orderItemList = objTestDataFactory.createTestUOMOrderItem(prodList,orderList[0].id, 'CS');
        for(Order_Item__c objorderItem : orderItemList){
            objorderItem.Estimated_Amount__c = 10;
            objorderItem.Order_Quantity__c = 1;
        }
        update orderItemList; 
                
        Order__c ordRec1 = new Order__c ();
        Test.setCurrentPageReference(new PageReference('Page.OrderBookingPageCX')); 
        System.currentPageReference().getParameters().put('order_id', orderList[0].Id);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(ordRec1);
        OrderBookingPageCX cx1 = new OrderBookingPageCX(sc1);
        cx1.isInitialLoad = TRUE;
        
        cx1.accountNumberMap.put(orderList[0].Account__c, orderList[1].Account__c);
        cx1.RDDDate = orderList[1].requested_Delivery_Date__c+1;
        cx1.renderDeliveryShipTo = FALSE;
        cx1.validateOrder(orderList[1], prodList);
        cx1.LastPage();
        cx1.FirstPage();
        cx1.btnProceed();
    }
}
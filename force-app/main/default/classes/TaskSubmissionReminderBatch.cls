/*
-----------------------------------------------------------------------------------------------------------
Modified By                Date          Version          Description
-----------------------------------------------------------------------------------------------------------
Eternus Solutions          19/07/2018    Initial          Batch class to send reminders to user if they have
                                                          not sent Month records to Approval or have not
                                                          created Month records
-----------------------------------------------------------------------------------------------------------

*/
// Run batch using : Id batchJobId = Database.executeBatch(new TaskSubmissionReminderBatch(), 200);
global class TaskSubmissionReminderBatch Implements Database.Batchable <sObject>, Schedulable, Database.Stateful
 {
    private Integer dayParam;


    public TaskSubmissionReminderBatch(Integer dayParameter) {
        dayParam = dayParameter;
    }

    global List<User> start(Database.BatchableContext bc) {

        DateTime currentTime = datetime.now().addMonths(1);
        String nextMonth = currentTime.format('MMMMM');
        String year = currentTime.format('YYYY');
        // Query to fetch all the month records which are submitted for approval
        String query = 'SELECT Id, Status__c, Month__c, Year__c, OwnerId FROM Month__c WHERE (Status__c = \'Approved\' OR Status__c = \'Submitted\')  AND '
                     + 'Month__c = \''+nextMonth+'\' AND Year__c = \''+year+'\'';
        List<Id> submissionDoneUserIds = new List<Id>();
        // Getting the Ids of User who have sent the Month record for Approval
        List<Month__c> monthList = Database.query(query);
        for(Month__c monthObj : monthList) {
            submissionDoneUserIds.add(monthObj.OwnerId);
        }
        List<User> userList = [SELECT Id, Email, FirstName FROM USER WHERE (Business_Unit__c = 'GFS') AND Profile.Name = 'Sales User' AND Id NOT IN : submissionDoneUserIds];
        if(Test.isRunningTest()){
            return [SELECT Id, Email, FirstName FROM User WHERE UserName LIKE 'smpfcTestUser@smpfc.com%'];
        }
        return userList;
    }

    global void execute(Database.BatchableContext bc, List<User> userList) {


        // Fetching the users who have not sent Month records for Approval or have not created Month records.

        System.debug('userList : '+userList);
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        //DateTime currentTime = datetime.now().addMonths(1);
        //String nextMonth = currentTime.format('MMMMM');
        //List<MCPReminder__c> mcpReminderList = [SELECT Id,
        //                                               Failed_MCP_reminder_sent__c,
        //                                               Failed_MCP_Submission_reminder_day__c,
        //                                               Reminder_before_business_days__c
        //                                               //,Reminder_email_sent__c
        //                                          FROM MCPReminder__c
        //                                          LIMIT 1];
        for(User user : userList) {
            List<String> toAddresses = new List<String>{user.Email};
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);

            String messageBody;
            //if(mcpReminderList[0].Failed_MCP_reminder_sent__c) {

            if(!test.isRunningTest()) {
                dayParam = Date.today().Day();
            }
            if(dayParam < Integer.valueOf(System.Label.Second_Reminder_Date) && dayParam >= Integer.valueOf(System.Label.Third_Reminder_Date)) {
                DateTime currentTime = datetime.now();
                String nextMonth = currentTime.format('MMMMM');
                mail.setSubject('Task Submission for Month '+nextMonth+' Failed');
                messageBody = '<html>'
                            + '<body>Hello ' + user.FirstName + ',<br/>'
                            + 'You have failed to submit your MCP for month '+nextMonth+' for approval.'
                            + '<br/><br/><b>Thanks & Regards,</b><br/>Salesforce Admin</body></html>';
            }
            else {
                DateTime currentTime = datetime.now().addMonths(1);
                String nextMonth = currentTime.format('MMMMM');
                mail.setSubject('Task Submission for Month '+nextMonth+' Pending');
                messageBody = '<html>'
                            + '<body>Hello ' + user.FirstName + ',<br/>'
                            + 'You have not yet submitted your MCP for month '+nextMonth+' for approval. Kindly submit it ASAP'
                            + '<br/><br/><b>Thanks & Regards,</b><br/>Salesforce Admin</body></html>';
            }
            System.debug('messageBody : '+messageBody);
            mail.setHtmlBody(messageBody);
            mailList.add(mail);
            System.debug('mail : '+mail);
        }
        if(mailList != null) {
            System.debug('Sending mail');
            Messaging.sendEmail(mailList);
        }
    }

    global void finish(Database.BatchableContext bc) {
        //List<MCPReminder__c> mcpReminderList = [SELECT Id,
        //                                               Failed_MCP_reminder_sent__c,
        //                                               Failed_MCP_Submission_reminder_day__c,
        //                                               Reminder_before_business_days__c
        //                                               //,Reminder_email_sent__c
        //                                          FROM MCPReminder__c
        //                                          LIMIT 1];
        DateTime scheduleDate;
        String cronExp;
        //if(mcpReminderList[0].Failed_MCP_reminder_sent__c) {
        if(dayParam < Integer.valueOf(System.Label.Second_Reminder_Date) && dayParam >= Integer.valueOf(System.Label.Third_Reminder_Date)) {
            //scheduleDate = addBusinessDays(System.Today().addMonths(1).toStartOfMonth(),Integer.valueOf(mcpReminderList[0].Failed_MCP_Submission_reminder_day__c), System.Today().addMonths(1));
            scheduleDate = System.Today().toStartOfMonth();
            if(String.isBlank(System.Label.Second_Reminder_Date)) {
                return;
            }
            cronExp = scheduleDate.format('ss mm 09 '+System.Label.Second_Reminder_Date+' MM ? yyyy');

            //mcpReminderList[0].Failed_MCP_reminder_sent__c = false;
            //mcpReminderList[0].Reminder_email_sent__c = true;
        }
        else {
            //scheduleDate = addBusinessDays(System.Today().addMonths(1).toStartOfMonth(),-Integer.valueOf(mcpReminderList[0].Reminder_before_business_days__c), System.Today());
            scheduleDate = System.Today().addMonths(1).toStartOfMonth();
            if(String.isBlank(System.Label.Third_Reminder_Date)) {
                return;
            }
            cronExp = scheduleDate.format('ss mm 09 '+System.Label.Third_Reminder_Date+' MM ? yyyy');
            //mcpReminderList[0].Failed_MCP_reminder_sent__c = true;
            //mcpReminderList[0].Reminder_email_sent__c = false;
        }
        //update mcpReminderList;
        System.debug('cronExp : '+cronExp);
        if(!test.isRunningTest()) {
            System.schedule( 'TaskSubmissionReminderBatch' + cronExp, cronExp, new TaskSubmissionReminderBatch(dayParam));
        }
    }

    global void execute(System.SchedulableContext sc) {
        Database.executeBatch(new TaskSubmissionReminderBatch(dayParam));
    }

    //public static Boolean IsWeekendDay(Date dateParam, List<Holiday__c> holidayList) {
    //    boolean result     = false;
    //    //Recover the day of the week

    //    Date startOfWeek   = dateParam.toStartOfWeek();
    //    Integer dayOfWeek  = dateParam.day() - startOfWeek.day();
    //    result = dayOfWeek == 0 || dayOfWeek == 6 ? true : false;
    //    System.debug('--result--'+result);
    //    System.debug('--dayOfWeek--'+dayOfWeek);
    //    System.debug('--holidayList--'+holidayList);

    //    if(!result && holidayList != null) {
    //        System.debug('--inside if--');
    //        for(Holiday__c h : holidayList) {
    //            System.debug('dateParam :: '+dateParam);
    //            System.debug('h.Date_of_Holiday__c :: '+h.Date_of_Holiday__c);
    //            if(h.Date_of_Holiday__c == dateParam) {
    //                result = true;
    //            }
    //        }
    //    }

    //    return result;
    //}

    //public static Date addBusinessDays(Date startDate, integer businessDaysToAdd , Date todayDateTime) {
    //    DateTime startDateTime = DateTime.newInstance(todayDateTime.year(), todayDateTime.month(), todayDateTime.day());
    //    String monthName = startDateTime.format('MMMMM');
    //    List<Holiday__c> holidayList = [SELECT Id, Date_of_Holiday__c FROM Holiday__c WHERE Month__c =: monthName AND Year__c =: String.valueOf(startDate.year())];
    //    //Add or decrease in BusinessDaysToAdd days
    //    Date finalDate = startDate;
    //    integer direction = businessDaysToAdd < 0 ? -1 : 1;
    //    while(businessDaysToAdd != 0)
    //    {
    //        finalDate = finalDate.AddDays(direction);
    //        if (!isWeekendDay(finalDate,holidayList))
    //        {
    //            businessDaysToAdd -= direction;
    //        }
    //    }
    //    return finalDate;
    //}
}
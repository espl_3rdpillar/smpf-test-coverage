@isTest
private class ContactReportFormTest {

    private static testMethod void insertContactReportDataTest() {
        Test.startTest();
        List<Contact_Report__c> listContactReport = DIL_TestDataFactory.getContactReports(1,true);
//        Id contRepId = DIL_TestDataFactory.getContactReports(1,true)[0].Id;
        String strJson = '{"territory":"Test TERRITORY","crDate":"10/4/2018","outletName":"Test OUTLET NAME","place":"Test PLACE","objectives":"Test OBJECTIVES","issuesConcerns":"Test ISSUES & CONCERNS","nextSteps":"Test NEXT STEPS","conforme":"Test CONFORME","id":"'+ listContactReport[0].Id +'","status":"Saved"}';
        String nextStepList = '[{"Actvitivies__c":"Test","Person_In_Charge__c":"Test","Target_Date__c":"10/24/2018"},{"Actvitivies__c":"Test","Person_In_Charge__c":"Test","Target_Date__c":"10/25/2018"}]';
        String discussionList = '[{"Topic__c":"Test","Agreements_Issues_Concerns__c":"TEst"},{"Topic__c":"Test","Agreements_Issues_Concerns__c":"Test"}]';
        List<String> lstAttendees = new List<String>{'User 1','User 2'};

        ContactReportForm.getAccountName(listContactReport[0].eForm__c);
        //ContactReportForm.insertContactReportData(strJson, lstAttendees);
        ContactReportForm.insertContactReportData(strJson, discussionList, nextStepList);
        Test.stopTest();
    }

    private static testMethod void insertContactReportDataTest2() {
        List<Contact_Report__c> listContactReport = DIL_TestDataFactory.getContactReports(1,true);

        List<Task> taskObj = DIL_TestDataFactory.getTasks(1, false);
        taskObj[0].eForm__c = listContactReport[0].eForm__c;
        taskObj[0].Check_in__c = System.Today();
        insert taskObj[0];
        taskObj[0].eForm__c = listContactReport[0].eForm__c;

        Test.startTest();
        update taskObj[0];
//        Id contRepId = DIL_TestDataFactory.getContactReports(1,true)[0].Id;
        String strJson = '{"territory":"Test TERRITORY","crDate":"10/4/2018","outletName":"Test OUTLET NAME","place":"Test PLACE","objectives":"Test OBJECTIVES","issuesConcerns":"Test ISSUES & CONCERNS","nextSteps":"Test NEXT STEPS","conforme":"Test CONFORME","id":"'+ listContactReport[0].Id +'","status":"Submitted"}';
        String nextStepList = '[{"Actvitivies__c":"Test","Person_In_Charge__c":"Test","Target_Date__c":"10/24/2018"},{"Actvitivies__c":"Test","Person_In_Charge__c":"Test","Target_Date__c":"10/25/2018"}]';
        String discussionList = '[{"Topic__c":"Test","Agreements_Issues_Concerns__c":"TEst"},{"Topic__c":"Test","Agreements_Issues_Concerns__c":"Test"}]';
        List<String> lstAttendees = new List<String>{'User 1','User 2'};

        ContactReportForm.getAccountName(listContactReport[0].eForm__c);
        //ContactReportForm.insertContactReportData(strJson, lstAttendees);
        ContactReportForm.insertContactReportData(strJson, discussionList, nextStepList);
        Test.stopTest();
    }

    private static testMethod void fetchUsersTest() {
        //List<ContactReportForm.SearchWrapper> lstSrchWrap = ContactReportForm.fetchUsers();
        Test.startTest();
        new ContactReportForm(null);
        Test.stopTest();
    }

    private static testMethod void getExistingContactReportTest() {

        List<Contact_Report__c> listContactReport = DIL_TestDataFactory.getContactReports(1,true);

        List<Task> taskObj = DIL_TestDataFactory.getTasks(1, false);
        taskObj[0].eForm__c = listContactReport[0].eForm__c;
        taskObj[0].Check_in__c = System.Today();
        insert taskObj[0];
        taskObj[0].eForm__c = listContactReport[0].eForm__c;
        Test.startTest();
        update taskObj[0];
        System.debug('taskObj :: '+taskObj[0]);

        ContactReportForm.ContRepWrapper objContRep = ContactReportForm.getExistingContactReport(listContactReport[0].Id);
        Test.stopTest();
    }

    private static testMethod void getExistingContactReportTest2() {
        Test.startTest();
        List<Contact_Report__c> listContactReport = DIL_TestDataFactory.getContactReports(1,true);

        ContactReportForm.ContRepWrapper objContRep = ContactReportForm.getExistingContactReport(listContactReport[0].Id);
        Test.stopTest();
    }

}
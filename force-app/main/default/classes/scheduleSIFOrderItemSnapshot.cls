/*********************************************************************
Author          Deloitte (RVillanueva)
Description     Schedulable class to run monthly and create SIF and order Item record
Date Created:   09/29/2016
*********************************************************************/
global class scheduleSIFOrderItemSnapshot implements Schedulable {
    global void execute(SchedulableContext SC) { 
        Integer yr = System.Today().year();     
        Date mnth = System.Today().addMonths(-3);
        Integer last3Month = mnth.month();
        if (mnth.month() > System.Today().month()) {
            yr = System.Today().year()-1;
        }
        System.debug('last3Month-----'+ last3Month + '--yr--'+ yr);
        
        batchSIFOrderItemSnapshot bc = new batchSIFOrderItemSnapshot(last3Month, yr); 
        database.executebatch(bc,350);    
    }   
}
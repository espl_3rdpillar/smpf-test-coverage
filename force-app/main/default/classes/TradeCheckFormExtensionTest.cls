@isTest
private class TradeCheckFormExtensionTest {

    @testSetup
    private static void init() {
        DateTime objDate = Datetime.newInstance(Date.newInstance(2016, 12, 9), Time.newInstance(23,59,0,0));
        List<Task> taskList = DIL_TestDataFactory.getTasks(1, false);
        taskList[0].Work_Type__c = 'Field Work';
        taskList[0].EndDateTime__c = objDate;
        insert taskList;
    }

    @isTest static void testGetExistingTradeCheckForExistingRecord() {

        List<Task> lstTask = [Select EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];
        Trade_Audit__c objTradeCheck= new Trade_Audit__c(EForm__c = lstTask[0].EForm__c, Status__c = 'Submitted');
        insert objTradeCheck;
        update objTradeCheck;

        Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objTradeCheck);
            TradeCheckFormExtension testTradeCheckFormExtension = new TradeCheckFormExtension(objStandardController);

            PageReference pageRef = Page.TradeAuditForm;
            pageRef.getParameters().put('id', String.valueOf(objTradeCheck.Id));
            Test.setCurrentPage(pageRef);
            Trade_Audit__c objTradeCheckrecieved = TradeCheckFormExtension.getExistingTradeCheck(objTradeCheck.Id);
        Test.stopTest();
        System.assert(objTradeCheckrecieved != null);
        System.assert(objTradeCheckrecieved.Id != null);
    }

    @isTest static void testGetExistingTradeCheckForNonExistingRecord() {

        List<Task> lstTask = [Select EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];
        Trade_Audit__c objTradeCheck= new Trade_Audit__c(EForm__c = lstTask[0].EForm__c, Status__c = 'Submitted');
        insert objTradeCheck;

        delete objTradeCheck;
        Test.startTest();
            ApexPages.StandardController objStandardController = new ApexPages.StandardController(objTradeCheck);
            TradeCheckFormExtension testTradeCheckFormExtension = new TradeCheckFormExtension(objStandardController);

            PageReference pageRef = Page.TradeAuditForm;
            pageRef.getParameters().put('id', String.valueOf(objTradeCheck.Id));
            Test.setCurrentPage(pageRef);
            Trade_Audit__c objTradeCheckrecieved = TradeCheckFormExtension.getExistingTradeCheck(objTradeCheck.Id);
        Test.stopTest();
        System.assert(objTradeCheckrecieved != null);
        System.assert(objTradeCheckrecieved.Id == null);
    }
}
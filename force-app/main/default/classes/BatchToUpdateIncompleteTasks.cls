/*
-------------------------------------------------------------------------------------------------------------------------
Modified By                Date          Version          Description
-------------------------------------------------------------------------------------------------------------------------
Eternus Solutions          26/09/2018    Initial          Batch class to update incomplete tasks
Purushottam Bhaigade       02/02/2019    v2               Add code to handle only single activity with no check out time 
                                                          in Multiple Account
-------------------------------------------------------------------------------------------------------------------------

*/
public class BatchToUpdateIncompleteTasks Implements Database.Batchable <sObject>,Schedulable
 {
    public Database.QueryLocator start(Database.BatchableContext bc) {

        String query = 'SELECT Id, Auto_Signed_Off__c, Check_in__c, Task_Completion_Datetime__c, Activity_Status__c, Account_Configuration__c, Account_Configuration__r.Accounts_Created__c, Account_Configuration__r.Activity_Status__c, Account_Configuration__r.Accounts_Left__c, Account_Configuration__r.Number_of_Accounts__c '
                     + 'FROM Task Where Work_Type__c != \'Leave\' AND StartDateTime__c = Yesterday AND Month__r.Status__c = \'Approved\' AND (Task_Completion_Datetime__c = null OR Check_out__c = null) ';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Task> taskList) {
        if(taskList != null) {
            Map<Id, Account_Configuration__c> accConfigMap = new Map<Id, Account_Configuration__c>();
            for(Task taskObj : taskList) {
                taskObj.Auto_Signed_Off__c = true;
                if(taskObj.Task_Completion_Datetime__c != null) {
                    taskObj.Activity_Status__c = 'Complete';
                    if(taskObj.Account_Configuration__c != null && taskObj.Account_Configuration__r.Accounts_Left__c == taskObj.Account_Configuration__r.Number_of_Accounts__c){
                        taskObj.Account_Configuration__r.Accounts_Created__c = (taskObj.Account_Configuration__r.Accounts_Created__c == null) ? 1 : taskObj.Account_Configuration__r.Accounts_Created__c + 1;
                        taskObj.Account_Configuration__r.Activity_Status__c = 'Incomplete';
                        accConfigMap.put(taskObj.Account_Configuration__c, taskObj.Account_Configuration__r);
                    }
                }
                else {
                    taskObj.Activity_Status__c = 'Incomplete';
                    if(taskObj.Account_Configuration__c != null) {
                        taskObj.Account_Configuration__r.Activity_Status__c = 'Incomplete';
                        accConfigMap.put(taskObj.Account_Configuration__c, taskObj.Account_Configuration__r);
                    }
                }
                
                DateTime dtForAutoSignOff = DateTime.newInstance(Date.today().adddays(-1),Time.newInstance(23,59,0,0));
                if(taskObj.Check_in__c == null) {
                    taskObj.Check_in__c = dtForAutoSignOff;
                }
                if(taskObj.Check_out__c == null){
                    taskObj.Check_out__c = dtForAutoSignOff;
                }
            }
            System.debug('accConfigMap :'+accConfigMap);
            System.debug('accConfigMap.values :'+accConfigMap.values());
            StaticResources.changesMadeByWebService = true;
                if(!taskList.isEmpty()) {
                    Database.update(taskList, false);
                }
                if(!accConfigMap.isEmpty()) {
                    Database.update(accConfigMap.values(), false);
                }
            StaticResources.changesMadeByWebService = false;
        }
    }

    public void finish(Database.BatchableContext bc) {
        Set<String> jobStatus    = new Set<String>{'Aborted', 'Completed', 'Failed'};
        String SCHEDULE_JOB_NAME = 'BatchToUpdateIncompleteTasks%';
        Set<String> jobState     = new Set<String>{'COMPLETE', 'ERROR', 'DELETED'};
        Boolean isJobRunning     = false;
        DateTime scheduleDate = System.today()+1;
        String cronExp = scheduleDate.format('ss 5 0 '+scheduleDate.day()+' MM ? yyyy');
        System.debug('---CRON---'+cronExp);
        List<AsyncApexJob> apexJobs = [SELECT Status FROM AsyncApexJob WHERE Status NOT IN :jobStatus
                                       AND ApexClassId IN (SELECT Id FROM ApexClass WHERE Name = 'BatchToUpdateIncompleteTasks')];
        // Query on Scheduled jobs.
        for(CronTrigger ct : [SELECT Id, State FROM CronTrigger WHERE CronJobDetail.Name LIKE :SCHEDULE_JOB_NAME]){
            // If state of the trigger is 'COMPLETE', 'ERROR', or 'DELETED'.
            if(jobState.contains(ct.State)) {
                if((ct.State).equalsIgnoreCase('COMPLETE') || (ct.State).equalsIgnoreCase('ERROR')) {
                    System.abortJob(ct.Id);
                } else {
                    // If no such records found which are executing then abort the current schedule and rescedule.
                    if(apexJobs.isEmpty()) {
                        System.abortJob(ct.Id);
                    } else { // Else job is running fine.
                        isJobRunning = true;
                    } // End of if - else.
                }
            } else { // Job is running fine.
                isJobRunning = true;
            } // End of if - else.

        } // End of for.

        if(!test.isRunningTest() && !isJobRunning) {
            System.schedule( 'BatchToUpdateIncompleteTasks' + cronExp, cronExp, new BatchToUpdateIncompleteTasks());
        }
    
        createEodRecs();
    }

    public void execute(System.SchedulableContext sc) {
        Database.executeBatch(new BatchToUpdateIncompleteTasks());
    }
    
    public static void createEodRecs(){
        
        List<Start_of_Day__c> lstSodRecs = [Select Id,OwnerId,(SELECT Id From End_of_Day__r) From Start_of_Day__c WHERE CreatedDate = YESTERDAY];//
        
        List<End_of_Day__c> lstEodRecsToInsert = new List<End_of_Day__c>();
        
        for(Start_of_Day__c objSod : lstSodRecs){
            
            if(objSod.End_of_Day__r.size() == 0){
                
                End_of_Day__c objEod = new End_of_Day__c();
                
                objEod.Start_of_Day__c = objSod.Id;
                objEod.OwnerId = objSod.OwnerId;
                objEod.Conducts_post_route_meeting__c = True;
                objEod.Discuss_issues_and_concerns__c = True;
                objEod.Document_issues_and_concerns__c = True;
                objEod.Leaves_for_home__c = True;
                objEod.Reviews_objectives__c = True;
                
                lstEodRecsToInsert.add(objEod);
            }
        }
        StaticResources.byPassStartOrEndOfDayTrigger = True;
        insert lstEodRecsToInsert;
        StaticResources.byPassStartOrEndOfDayTrigger = False;
    }
}
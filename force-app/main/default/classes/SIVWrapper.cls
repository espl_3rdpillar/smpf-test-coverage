public class SIVWrapper {

public Id accountId {get; set;}
public Id productId {get; set;}

public string buCode {get; set;}
public string distributorCode {get; set;}
public string skuCode {get; set;}
public string uomName {get; set;}
public string channelCode {get; set;}

public Inventory__c Inv {get; set;}
public Inventory_Item__c Item {get; set;}
public Inventory_Line__c Line {get; set;}
}
/*
-----------------------------------------------------------------------------------------------------------
Modified By                Date          Version          Description
-----------------------------------------------------------------------------------------------------------
Eternus Solutions          23/06/2018    Initial          Test class for Month Trigger handler
-----------------------------------------------------------------------------------------------------------

*/
@isTest
public class MonthTriggerHandlerTest {

	// Method to create Test Accounts
	public static List<Account> createTestAccount(Integer num) {
		List<Account> accountList = new List<Account>();
		for(Integer i=1; i<=num; i++) {
			accountList.add(new Account(Name='Test Account '+i, AccountNumber = '' +i));
		}
		return accountList;
	}

	// Method to create test Month records
	public static List<Month__c> createMonth(Integer num) {
		List<Month__c> monthList = new List<Month__c>();
		for(Integer i=1; i<=num; i++) {
			monthList.add(new Month__c(Month__c='April'
                                       , Year__c='2019'));
		}
		return monthList;
	}

    // Method to create Holiday records
    public static List<Holiday__c> createHoliday(Integer num) {
        List<Holiday__c> holidayList = new List<Holiday__c>();
        for(Integer i=1; i<=num; i++) {
            holidayList.add(new Holiday__c(Reason_of_Holiday__c = 'Sample reason', Date_of_Holiday__c = Date.newInstance(2019, 4, i)));
        }
        return holidayList;
    }


	// Method to create test Task Records
	public Static List<Task> createTasks(Integer num
                                         , String accId) {
		List<Task> taskList = new List<Task>();
		for(Integer i=1; i<=num; i++) {
			taskList.add(new Task( User__c = UserInfo.getUserId()
                                 , Related_Account__c = accId
                                 , StartDateTime__c = Datetime.newInstance(2019, 4, i)
                                 , EndDateTime__c = Datetime.newInstance(2019, 4, i)
                                 //, All_Day__c = true
                                 , Title__c = 'Task '+i
                                 , Status = 'In Progress')
                                 );
		}
		return taskList;
	}

     // Method to create test Account Configuration records
    public static List<Account_Configuration__c> createTestConfigurations(Integer num, Datetime startDate) {
        List<Account_Configuration__c> accConfigList = new List<Account_Configuration__c>();
        for(Integer i=0; i< num; i++) {
            accConfigList.add(new Account_Configuration__c( Number_of_Accounts__c = i+1
                                                          , StartDateTime__c = startDate.addDays(i)
                                                          , EndDateTime__c = startDate.addDays(i).addHours(8)
                                                          , User__c = UserInfo.getUserId()
                                                          , Title__c = 'Test Title'
                                                          , Subject__c = 'Test Subject'
                                                          , Status__c = 'New'
                                                          ));
        }
        return accConfigList;
    }

    // Method to create test Task Records
    public Static List<Task> createTaskData( Integer num
                                        , String accId
                                        , DateTime startDate
                                        , Boolean checkin
                                        , Boolean checkout
                                        , String workType
                                        ) {
        List<Task> taskList = new List<Task>();
        for(Integer i=1; i<=num; i++) {
            Task taskObj = new Task();
            taskObj.Related_Account__c = accId;
            taskObj.StartDateTime__c = startDate.addDays(i);
            taskObj.EndDateTime__c = startDate.addDays(i).addHours(8);
            //taskObj.All_Day__c = true;
            taskObj.Title__c = 'Task '+i;
            taskObj.Status = 'In Progress';
            taskObj.User__c = UserInfo.getUserId();
            taskObj.Work_Type__C = workType;
            if(checkin)
                taskObj.Check_in__c = startDate.addDays(i).addMinutes(10);
            if(checkout)
                taskObj.Check_out__c = startDate.addDays(i).addMinutes(40);
            taskList.add(taskObj);
        }
        return taskList;
    }

	@isTest static void approveTasks() {
        List<Holiday__c> holidayList = createHoliday(10);
        insert holidayList;

		List<Account> accList = createTestAccount(10);
		insert accList;

		//List<Month__c> monthList = createMonth(10);
		//insert monthList;

		List<Task> taskList = createTasks(10, accList[0].Id);
		insert taskList;

        List<Task> tasksCreated = [SELECT Id
                                        , Month__c
                                     FROM Task
                                    WHERE Id IN: taskList];
        List<Id> monthIdList = new List<Id>();
        for(Task taskObj : tasksCreated) {
            monthIdList.add(taskObj.Month__c);
        }
        System.debug('tasksCreated :: '+tasksCreated);
        List<Month__c> monthList = [SELECT Id, Status__c FROM Month__c WHERE Id IN: monthIdList];
		Test.StartTest();
		monthList[0].Status__c = 'Approved';
		update monthList;
		Test.StopTest();

		System.debug('Month : '+monthList[0]);
		List<Task> taskListUpdated = [SELECT Id, Approved__c, WhatId, Status FROM Task WHERE Month__c =: monthList[0].Id ];
        System.debug('taskListUpdated :: '+taskListUpdated);
		System.assertEquals(taskListUpdated[0].Approved__c,true);
		System.assertEquals(taskListUpdated[0].WhatId,accList[0].Id);
	}

    public static testMethod void getMonthTest() {

        List<String> monthList = new List<String>{ 'January'
                                                 , 'February'
                                                 , 'March'
                                                 , 'April'
                                                 , 'May'
                                                 , 'June'
                                                 , 'July'
                                                 , 'August'
                                                 , 'September'
                                                 , 'October'
                                                 , 'November'
                                                 , 'December'
                                                 , 'Westerday'
                                                 };

        System.Test.startTest();
            for(String monthStr : monthList) {
                Integer res = MonthTriggerHandler.getMonth(monthStr);
                System.assertNotEquals(null, res);
            }
        System.Test.stopTest();

    }

    public static testMethod void revertTasksIfRejectedTest(){

        Month__c objMonth = createMonth(1)[0];
        objMonth.Status__c = 'Not Submitted';

        insert objMonth;


        List<Account> accList = createTestAccount(10);
		insert accList;

        //List<Account_Configuration__c> configList = createRecurringConfig(5, 2, 1);
        //insert configList;

		List<Task> taskList = createTasks(10, accList[0].Id);
		for(Task objTask : taskList){
		    objTask.Month__c = objMonth.Id;
		}

		insert taskList;

        //////////////////////

            List<ContentVersion> lstContentVersion = new List<ContentVersion>();
            for(Task objTask : taskList){
                //String objTaskSerialized = Json.serialize(objTask);//wrong state
                String objTaskSerialized = Json.serialize(objTask);
                //Logic for creating a file
                ContentVersion conVer = new ContentVersion();
                conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
                conVer.PathOnClient = objTask.Id+'.txt'; // The files name, extension is very important here which will help the file in preview.
                conVer.Title = objTask.Id; // Display name of the files
                conVer.VersionData = Blob.ValueOf(objTaskSerialized); // converting your binary string to Blog
                lstContentVersion.add(conVer);
            }
            System.debug('lstContentVersion--->'+lstContentVersion);
            insert lstContentVersion;


            Set<Id> setCVIds = new Set<Id>();
            for(ContentVersion objCV : lstContentVersion){
                setCVIds.add(objCV.Id);
            }

            List<ContentVersion> lstfetchedCVs = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE Id IN :setCVIds];

            Map<Id,Id> mapCvIdToCdId = new Map<Id,Id>();
            for(ContentVersion objCV : lstfetchedCVs){
                mapCvIdToCdId.put(objCV.Id,objCV.ContentDocumentId);
            }

            List<ContentDocumentLink> lstContentDocumentLink = new List<ContentDocumentLink>();
            for(ContentVersion objCV : lstContentVersion){
                ContentDocumentLink cDe = new ContentDocumentLink();
                cDe.LinkedEntityId = Id.ValueOf(objCV.Title);
                cDe.ContentDocumentId = mapCvIdToCdId.get(objCV.Id);
                cDe.ShareType = 'V';
                cDe.Visibility = 'AllUsers';
                lstContentDocumentLink.add(cDe);
            }

            insert lstContentDocumentLink;


        //////////////////////



        /*
        List<Task> lstTestTasks = [SELECT Id,Month__c,Month__r.Resubmission_Date__c,Month__r.Status__c,Status FROM Task];
		for(Task ObjTask : lstTestTasks){
		    ObjTask.Status = 'Rejected';
		    //System.debug('In test class resub date--->'+ObjTask.Month__r.Resubmission_Date__c);
		    //System.debug('In test class status--->'+ObjTask.Month__r.Status__c);
		}
        update lstTestTasks;
        */

        objMonth.Status__c = 'Submitted';
        objMonth.Resubmission_Date__c = Date.today();
        update objMonth;

        Test.startTest();
        objMonth.Status__c = 'Rejected';
        System.debug('----Check after this----');
        update objMonth;
        Test.stopTest();

        System.debug('data2-->'+[Select Resubmission_Date__c,Status__c FROM Month__c]);

    }


    @isTest static void overlappingTaskTest() {
        List<Account> accountList = createTestAccount(5);
        insert accountList;

        List<Task> taskListExisting = createTaskData(5, accountList[0].Id, System.now(), true, true, 'Office work');
        insert taskListExisting;

        List<Account_Configuration__c> accConfigListExisting = createTestConfigurations(10, System.now());
        for(Account_Configuration__c accConfigObj : accConfigListExisting) {
            accConfigObj.Accounts_Created__c = 1;
        }
        insert accConfigListExisting;
        Set<Id> monthId = new Set<Id>();
        Test.startTest();
        for( Task taskObj : [SELECT Id, Month__c FROM Task WHERE Id IN: taskListExisting]) {
            monthId.add(taskObj.Month__c);
        }

        List<Month__c> monthList = [SELECT Id FROM Month__c WHERE Id IN: monthId];
        update monthList;
        Test.stopTest();
    }
}
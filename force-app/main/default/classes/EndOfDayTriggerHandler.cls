public class EndOfDayTriggerHandler {
    public void populateStartOfDay(List<End_of_Day__c> newEndOfDayList) {
        
        Map<Id,End_of_Day__c> mapEndOfDay = new Map<Id,End_of_Day__c>();
        
        for(End_of_Day__c endOfDayObj : newEndOfDayList) {
            mapEndOfDay.put(endOfDayObj.OwnerId, endOfDayObj);
        }
        
        for(Start_of_Day__c startOfDayObj : [SELECT Id,
                                                    OwnerId
                                               FROM Start_of_Day__c
                                              WHERE OwnerId IN : mapEndOfDay.keySet()
                                                AND CreatedDate = TODAY
                                           ORDER BY CreatedDate]) {
            mapEndOfDay.get(startOfDayObj.OwnerId).Start_of_Day__c = startOfDayObj.Id;
        }
    }
}
@isTest
private class ManualBatchForInventory_Test{
    @testSetup static void setupData(){
        List<Conversion__c> ConversionforInsertList = new List<Conversion__c>();
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        List<Custom_Product__c> parentProductList = new List<Custom_Product__c>();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'NATIONAL',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true
            );
        insert settings;
        Custom_Product__c prodParent1 = new Custom_Product__c();
        prodParent1.Name = 'ParentProductFeeds';
        prodParent1.Business_Unit__c = 'Feeds';
        prodParent1.Feeds__c = true;
        prodParent1.Market__c = marketList.get(0).Id;
        prodParent1.SKU_Code__c = '111';
        parentProductList.add(prodParent1);
        
        Custom_Product__c prodParent2 = new Custom_Product__c();
        prodParent2.Name = 'ParentProductGFS';
        prodParent2.Business_Unit__c = 'GFS';
        prodParent2.Feeds__c = true;
        prodParent2.Market__c = marketList.get(1).Id;
        prodParent2.SKU_Code__c = '222';
        parentProductList.add(prodParent2);
        
        if(parentProductList.size()>0){
            insert parentProductList;
        }
        Id distributorRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'Sample Grocery3';
        acc.Sales_Organization__c  = 'Feeds';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.AccountNumber = 'ACT31';
        insert acc;
        
        Inventory__c inventoryRecord = new Inventory__c();
        inventoryRecord.Account__c = acc.Id;
        inventoryRecord.Last_Update__c = Date.today();
        insert inventoryRecord;
        
        Inventory_Item__c invItem = new Inventory_Item__c();
        invItem.Inventory__c = inventoryRecord.Id;
        invItem.Product__c = parentProductList.get(0).Id;
        invItem.Quantity__c = 100;
        insert invItem;
        
        for(Custom_Product__c customProduct : parentProductList){
            Conversion__c conversionRecord = new Conversion__c(
                Name = 'KG',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecord);
            Conversion__c conversionRecordPC = new Conversion__c(
                Name = 'PC',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1
                );
            ConversionforInsertList.add(conversionRecordPC);
        }
        if(ConversionforInsertList.size()>0){
            insert ConversionforInsertList;
        }
        List<Inventory_Line__c> inventoryLineList = new List<Inventory_Line__c>();
        Inventory_Line__c invLine = new Inventory_Line__c();
        invLine.Inventory_Item__c = invItem.Id;
        invLine.UOM__c = ConversionforInsertList.get(0).Id;
        invLine.Depleted__c = true;
        invLine.Validate_Quantity__c = true;
        invLine.Validated_Quantity__c = 100;
        invLine.UpdatedBegBalance__c = false;
        inventoryLineList.add(invLine);
        
        Inventory_Line__c invLine2 = new Inventory_Line__c();
        invLine2.Inventory_Item__c = invItem.Id;
        invLine2.UOM__c = ConversionforInsertList.get(0).Id;
        invLine2.Depleted__c = true;
        invLine2.SIF_Quantity__c = 10;
        invLine2.Validate_Quantity__c = false;
        invLine2.Validated_Quantity__c = 100;
        invLine2.UpdatedBegBalance__c = false;
        inventoryLineList.add(invLine2);
        if(inventoryLineList.size()>0){
            insert inventoryLineList;
        }
        
        Monthly_Inventory_Snapshot__c snapshotSettings = new Monthly_Inventory_Snapshot__c();
        snapshotSettings.Name = 'Account';
        snapshotSettings.Source_Field__c = 'Account__c';
        snapshotSettings.Target_Field__c = 'Account__c';
        insert snapshotSettings;
        
    }
    private static testMethod void testCallSnapshot() {
        Test.startTest();
            //CallSnapshotBatch
            ManualBatchForInventory.callSnapshotBatchJob();
        Test.stopTest();
    }
    
    private static testMethod void testCallDeplete() {
        Test.startTest();
            //CallSnapshotBatch
            ManualBatchForInventory.callDepletedBatchJob();
        Test.stopTest();
    }
    
    private static testMethod void testCallUpdate() {
        Test.startTest();
            //CallSnapshotBatch
            ManualBatchForInventory.callBatchJob();
        Test.stopTest();
    }

}
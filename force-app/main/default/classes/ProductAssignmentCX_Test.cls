@isTest
private class ProductAssignmentCX_Test{
    
    static testMethod void AssgnProduct () {
    
        TestDataFactory data = new TestDataFactory();
               
        List<Account> distributorAccount = data.createTestAccount(1, True, false, 'Distributor/ Direct',null);
        List<Account> customerAccount = data.createTestAccount(1, True, True, 'Distributor Customer',distributorAccount[0].id);
        Id pricingDataType = Schema.SObjectType.Pricing_Condition__c.getRecordTypeInfosByName().get('Pricing').getRecordTypeId();
        Id discountDataType = Schema.SObjectType.Pricing_Condition__c.getRecordTypeInfosByName().get('Discount').getRecordTypeId();
        Id listPriceDatype = Schema.SObjectType.Pricing_Condition_Data__c.getRecordTypeInfosByName().get('List Price').getRecordTypeId();
        Id dspRecordtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Pricing_Condition__c prcingCondition = new Pricing_Condition__c(Name = 'TetPricingCondition');
        Product_Assignment__c newProductAssinment = new Product_Assignment__c();
        
        Contact newDSP = new Contact(Lastname = 'test lastName', Account = distributorAccount[0],RecordTypeId = dspRecordtype  );
        insert newDSP;
        
        
        
        List<Custom_Product__c> productList = data.createTestProducts(200);
        system.debug('@@ productList:'+productList);
        
        Test.StartTest();
        system.assertEquals(true,newDSP!=null);
        system.debug('## newDSP:'+newDSP);
        //create pricing condition record 
        
            PageReference pageRef = Page.ProductAssignmentPage;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller pageController = new ApexPages.Standardcontroller(newProductAssinment);
            ProductAssignmentCX pagExtension = new ProductAssignmentCX(pageController);
            pagExtension.currentProductAssignentRec.Account__c = customerAccount[0].id;
            pagExtension.InitializePage();
            system.debug('@@ productResults size['+pagExtension.productResults.size()+']:'+pagExtension.productResults);
            
            pagExtension.GoToNextPage();
            pagExtension.GoToPreviousPage();
            
            pagExtension.productsToDisplay[0].isSelected=true;
            
            pagExtension.filterByName ='x';
            pagExtension.filterByBU ='x';
            pagExtension.filterBySKU='x';
            pagExtension.filterByBrand='x';
            pagExtension.filterByCategory='x';
            pagExtension.SearchProducts();
            
            
            pagExtension.SaveProductAssignments();
            
            
            
        Test.StopTest();
    }
}
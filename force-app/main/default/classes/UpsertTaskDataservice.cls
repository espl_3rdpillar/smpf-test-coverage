@RestResource(urlMapping='/insertTaskData/*')
global with sharing class UpsertTaskDataservice {

    @HttpPost
    global static Wrappers.TaskResponseWrapper doPost() {

        RestRequest  request    = RestContext.request;
        RestResponse response   = RestContext.response;

        List<Task> taskUpsertList = new List<Task>();
        Map<Id, Task> taskMap = new Map<Id, Task>();

        String taskStr = (request.requestBody).toString();

        Map<String, Object> jsonMap;

        try {
            jsonMap = (Map<String, Object>)JSON.deserializeUntyped(taskStr);
        } catch(Exception e) {

            return createErrorMsg(e.getMessage(), null, taskStr);

        }

        System.debug('----->>>>' + jsonMap);

        //List<Task> taskJsonObjList = (List<Task>) JSON.deserialize(jsonMap.get('task'), List<Task>.class);


        for(Object obj : (List<Object>) jsonMap.get('task')) {

            String jsonTempStr = JSON.serialize(obj);
            System.debug('jsonTempStr :' + jsonTempStr);
            Task sObj = (Task) JSON.deserialize(jsonTempStr, Task.class);
            taskUpsertList.add(sObj);

        }

        try {

            StaticResources.changesMadeByWebService = true;
            upsert taskUpsertList;
            StaticResources.changesMadeByWebService = false;

            for(Task taskItr : [SELECT Id, EForm__r.Report_Ids__c FROM Task WHERE Id IN :taskUpsertList]) {
                taskMap.put(taskItr.Id, taskItr);
            }

            Wrappers.TaskResponseWrapper wrapperResObj  = new Wrappers.TaskResponseWrapper();
            wrapperResObj.success                       = true;
            wrapperResObj.jsonStr                       = taskStr;
            wrapperResObj.timeStamp                     = DateTime.now();
            wrapperResObj.eformNameVsEformPrefix        = new Map<String, String>();
            wrapperResObj.taskList                      = new List<Task>();

            wrapperResObj.eformNameVsEformPrefix.put('Contact_Report__c', Schema.getGlobalDescribe().get('Contact_Report__c').getDescribe().getKeyPrefix());
            wrapperResObj.eformNameVsEformPrefix.put('MOM__c', Schema.getGlobalDescribe().get('MOM__c').getDescribe().getKeyPrefix());
            wrapperResObj.eformNameVsEformPrefix.put('Service_Level__c', Schema.getGlobalDescribe().get('Service_Level__c').getDescribe().getKeyPrefix());
            wrapperResObj.eformNameVsEformPrefix.put('Trade_Visit__c', Schema.getGlobalDescribe().get('Trade_Visit__c').getDescribe().getKeyPrefix());
            wrapperResObj.eformNameVsEformPrefix.put('Trade_Audit__c', Schema.getGlobalDescribe().get('Trade_Audit__c').getDescribe().getKeyPrefix());


            for(Task taskItr : taskUpsertList) {

                //if(null == wrapperResObj.taskList) {
                    if(taskMap.containsKey(taskItr.Id)) {
                        taskItr.EForm__r = taskMap.get(taskItr.Id).EForm__r;
                    }

                    //continue;
                //}

                wrapperResObj.taskList.add(taskItr);

            }

            return wrapperResObj;

        } catch(Exception e) {

            System.debug('exception :' + e);

            return createErrorMsg(e.getMessage(), null, taskStr);

        }

    }

    private static Wrappers.TaskResponseWrapper createErrorMsg(String msgStr, List<Task> taskListPar, String jsonStrPar) {

        Wrappers.TaskResponseWrapper wrapperResObj  = new Wrappers.TaskResponseWrapper();
        wrapperResObj.success                       = false;
        wrapperResObj.jsonStr                       = jsonStrPar;
        wrapperResObj.errorMsg                      = new List<String>{msgStr};
        wrapperResObj.taskList                      = taskListPar;
        wrapperResObj.timeStamp                     = DateTime.now();
        wrapperResObj.eformNameVsEformPrefix        = new Map<String, String>();

        return wrapperResObj;

    }

}
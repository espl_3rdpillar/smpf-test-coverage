global class WebServiceResponse {

    global class ProductResponse{
        global Id productId {get;set;}
        global String uomName {get;set;}
        global Decimal conversionRate {get;set;}
        global Decimal csConversionRate {get;set;}
        global String skuCode {get;set;}
        global String name {get;set;}
        global Id category {get;set;}

        global ProductResponse(Id productId, String uomName, Decimal conversionRate, String skuCode, String name,Id category){
            this.productId = productId;
            this.uomName = uomName;
            this.conversionRate = conversionRate;
            this.skuCode = skuCode;
            this.name = name;
            this.category = category;
        }
        
        global ProductResponse(Id productId, String uomName, Decimal conversionRate, Decimal csConversionRate,String skuCode, String name,Id category){
            this.productId = productId;
            this.uomName = uomName;
            this.conversionRate = conversionRate;
            this.csConversionRate = csConversionRate;
            this.skuCode = skuCode;
            this.name = name;
            this.category = category;
        }
    }

	global class OrderItemResponse{
        global List<Id> ids {get;set;}
        global Boolean success {get;set;}
        global List<String> errors {get;set;}

        global OrderItemResponse(List<Id> ids, Boolean success, List<String> errors){
            this.ids = ids;
        	this.success = success;
        	this.errors = errors;
        }
    }

    global class OrderRequest{
        global String order {get;set;}
        global String orderItems {get;set;}
        global OrderRequest(String order, String orderItems){
        	this.order = order;
            this.orderItems = orderItems;
        }
    }

    global class OrderResponse {

        global Id orderId {get;set;}
        global List<Id> ids {get;set;}
        global Boolean success {get;set;}
        global List<String> errors {get;set;}
        global String sfoId;
        global String dateToday;
        global String lastModifiedDate;
        global String status;
        global String accountId;
        global String poNumber;
        global String soupEntryId;
        global String offlineCreatedDate;

        global OrderResponse(Id orderId, List<Id> ids, Boolean success, List<String> errors){
            this.orderId = orderId;
            this.ids = ids;
        	this.success = success;
        	this.errors = errors;
        }

        global OrderResponse( Id orderId
                            , List<Id> ids
                            , Boolean success
                            , List<String> errors
                            , String sfoId
                            , String dateToday
                            , String lastModifiedDate
                            , String status
                            , String accountId
                            , String poNumber
                            , String soupEntryId
                            ) {
            this.orderId           = orderId;
            this.ids               = ids;
            this.success           = success;
            this.errors            = errors;
            this.sfoId             = sfoId;
            this.dateToday         = dateToday;
            this.lastModifiedDate  = lastModifiedDate;
            this.status            = status;
            this.accountId         = accountId;
            this.poNumber          = poNumber;
            this.soupEntryId       = soupEntryId;

        }

        global OrderResponse( Id orderId
                            , List<Id> ids
                            , Boolean success
                            , List<String> errors
                            , String sfoId
                            , String dateToday
                            , String lastModifiedDate
                            , String status
                            ) {
            this.orderId           = orderId;
            this.ids               = ids;
            this.success           = success;
            this.errors            = errors;
            this.sfoId             = sfoId;
            this.dateToday         = dateToday;
            this.lastModifiedDate  = lastModifiedDate;
            this.status            = status;
        }

        global OrderResponse( Id orderId
                            , List<Id> ids
                            , Boolean success
                            , List<String> errors
                            , String sfoId
                            , String dateToday
                            , String lastModifiedDate
                            , String status
                            , String offlineCreatedDate
                            ) {
            this.orderId            = orderId;
            this.ids                = ids;
            this.success            = success;
            this.errors             = errors;
            this.sfoId              = sfoId;
            this.dateToday          = dateToday;
            this.lastModifiedDate   = lastModifiedDate;
            this.status             = status;
            this.offlineCreatedDate = offlineCreatedDate;
        }

        global OrderResponse( String accountId
                            , String poNumber
                            ) {

            this.accountId = accountId;
            this.poNumber  = poNumber;
        }
    }
}
global class UpdateChildRequisitionParentDependency implements Database.Batchable<Sobject>
{
    private String MotherKeyReference;
    
    public UpdateChildRequisitionParentDependency(String MotherEpawReference)
    {
        MotherKeyReference = MotherEpawReference;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        // Query Parent Requisition regardless of the Status in order for it to be binded by the Child Requisition. [Approval Status Inheritance]
        // 1. Get Requisition Approval Status that are not "New" so that the query will not be overloaded with useless data.
        // 2. Batch Update checker is used to avoid redundant update of Requisition even though it has been already approved.
        return Database.getQueryLocator([SELECT Mother_ePAW_Reference__c,
											    Approval_Status__c
									     FROM   ePAW_Form__c
									     WHERE  Parent_Requisition__c = true
									     AND    Mother_ePAW_Reference__c = :MotherKeyReference]);
    }

    global void execute(Database.BatchableContext bc, List<ePAW_Form__c> ParentRequisition)
    {
        List<ePAW_Form__c> ProcessForUpdate      = new List<ePAW_Form__c>();
        Set<String> MotherReference              = new Set<String>();
        Map<String, String> ApprovalStatusParent = new Map<String, String>();

        // Get all details first from Mother Requisition.
        for(ePAW_Form__c UpdateStatusChildRequisition : ParentRequisition)
        {
            MotherReference.add(UpdateStatusChildRequisition.Mother_ePAW_Reference__c); // Store Mother Reference Key in a Set that will be used to retrieve child requisitions.
            
            // Bind Mother Reference Key and Status on Map in order to update Child Requisition.
            ApprovalStatusParent.put(UpdateStatusChildRequisition.Mother_ePAW_Reference__c, UpdateStatusChildRequisition.Approval_Status__c);
        }
        
        // Bind Child Requisition with Mother Requisition by Mother ePAW Reference.
        System.debug('--- Querying Child Requisition(s) ---');
        for(ePAW_Form__c ChildRequisitionDetails : [SELECT Mother_ePAW_Reference__c, 
                                                           Approval_Status__c 
                                                    FROM   ePAW_Form__c 
                                                    WHERE  Child_Requisition__c = true
                                                    AND    Mother_ePAW_Reference__c IN :MotherReference])
        {
            System.debug(ChildRequisitionDetails); // Verify properly the Child Requisitions Retrieved from the query.
            ProcessForUpdate.add(new ePAW_Form__c(Id = ChildRequisitionDetails.Id, Approval_Status__c = ApprovalStatusParent.get(ChildRequisitionDetails.Mother_ePAW_Reference__c)));
        }
        System.debug('Child Requisition for Update: ' + ProcessForUpdate);
        System.debug('--- [EOF] Querying Child Requisition(s) ---');
        
        if(!ProcessForUpdate.isEmpty())
        {
            update ProcessForUpdate;
        }
    }    

    global void finish(Database.BatchableContext bc) 
    { 
        // Update Batch Update field for Parent Requisition once it is approved.
        List<ePAW_Form__c> UpdateParentRequisition = new List<ePAW_Form__c>();
        
        for(ePAW_Form__c ParentRequisition : [SELECT Name,
                                                     Batch_Update__c
                                              FROM   ePAW_Form__c 
                                              WHERE  (Approval_Status__c   = 'Approved'
                                              OR	  Approval_Status__c   = 'Cancelled')
                                              AND    Parent_Requisition__c = true 
                                              AND    Batch_Update__c       = false])
        {
            System.debug(ParentRequisition);
            UpdateParentRequisition.add(new ePAW_Form__c(Id = ParentRequisition.Id, Batch_Update__c = true));
        }
        System.debug('Parent Requisition for Update (Update Batch Update Checkbox): ' + UpdateParentRequisition);
        
        if(!UpdateParentRequisition.isEmpty())
        {
            update UpdateParentRequisition;
        }
    }    
}
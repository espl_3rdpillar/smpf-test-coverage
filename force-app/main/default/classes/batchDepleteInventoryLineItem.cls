global class batchDepleteInventoryLineItem implements Database.Batchable<sObject>, Schedulable{

    global void execute(SchedulableContext SC) {
      Database.executeBatch(new batchDepleteInventoryLineItem (), 2000);
   }


   global Database.querylocator start(Database.BatchableContext BC){         
       return Database.getQueryLocator([Select Id,Depleted__c
                                            From Inventory_Line__c
                                            Where Depleted__c = true]);
   }//end method  
   
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Inventory_Line__c> lineItemList = new List<Inventory_Line__c>();
        for(sObject s : scope){
            lineItemList.add((Inventory_Line__c)s);        
        }                
        
        if (lineItemList.size() > 0)
            delete lineItemList;
            
   }  // end method
   
   global void finish(Database.BatchableContext BC){     
   }//end method 
   
}//end class
@RestResource(urlMapping='/insertAuditLogData/*')
global with sharing class InsertAuditLogDataservice {
    @HttpPost
    global static void doPost() {

        RestRequest  request    = RestContext.request;
        RestResponse response   = RestContext.response;

        Map<String, Object> resMap = new Map<String, Object>();
        String reqStr = (request.requestBody).toString();
        List<SObject> jsonList = new List<SObject>();
        List<SObject> insertJsonList = new List<SObject>();
        Map<Id, SObject> updateJsonMap = new Map<Id, SObject>();

        try {
            jsonList = (List<SObject>)JSON.deserialize(reqStr, List<SObject>.class);
            System.debug('----' + jsonList);
        } catch(Exception e) {

            response.addHeader('Content-Type', 'application/json');
            response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object> { 'errorMsg'  => e.getMessage()
                                                                                        , 'eformList' => null
                                                                                        , 'success'   => false
                                                                                        }));
            response.statusCode = 400;
            return;
        }

        System.debug('----->>>>' + jsonList);

        try {

            for(SObject sobjectItr : jsonList) {
                if(sobjectItr.get('Id') != null && String.isNotBlank(String.valueOf(sobjectItr.get('Id')))) {
                    Id tempId = (Id)sobjectItr.get('Id');
                    updateJsonMap.put(tempId, sobjectItr);
                } else {
                    sobjectItr.put('Id', null);
                    insertJsonList.add(sobjectItr);
                }
            }

            insert insertJsonList;
            update updateJsonMap.values();

            resMap.put('success', true);
            resMap.put('timeStamp', DateTime.now());

            List<SObject> sobjectListToAdd = new List<SObject>();
            for(SObject sobjectItr : insertJsonList) {
                sobjectListToAdd.add(sobjectItr);
            }

            for(SObject sobjectItr : updateJsonMap.values()) {
                sobjectListToAdd.add(sobjectItr);
            }

            resMap.put('eformList', sobjectListToAdd);

        } catch(Exception e) {

            response.addHeader('Content-Type', 'application/json');
            response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object> { 'errorMsg'  => e.getMessage()
                                                                                        , 'eformList' => null
                                                                                        , 'success'   => false
                                                                                        }));
            response.statusCode = 400;
            return;
        }

        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(resMap));
        response.statusCode = 200;
    }
}
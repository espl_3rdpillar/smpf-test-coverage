/**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : BATCH CLASS FOR DELETING SIV STAGING RECORDS.
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.24.2016 - CREATED.
**/
global class DeleteSIVStagingRecords_Batch implements Database.Batchable<sObject>, Schedulable{

    global void execute(SchedulableContext SC) {
      Database.executeBatch(new DeleteSIVStagingRecords_Batch(), 2000);
   }


   global Database.querylocator start(Database.BatchableContext BC){         
       return Database.getQueryLocator([SELECT Id FROM SIV_Staging__c]);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> sobjectList){
        List<SIV_Staging__c> stagingList = new List<SIV_Staging__c>();
        for(SIV_Staging__c siv : (List<SIV_Staging__c>)sobjectList){
            stagingList.add(siv);        
        }                
        if(stagingList.size()>0){
            delete stagingList;
        }
            
   }
   global void finish(Database.BatchableContext BC){
       
   }
   
}
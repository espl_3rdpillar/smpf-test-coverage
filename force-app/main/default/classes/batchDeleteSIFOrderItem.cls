/*********************************************************************
Author          Deloitte (RVillanueva)
Description     Batch class to run monthly and Delete SIF and order Item record already created as csv file
Date Created:   09/29/2016
*********************************************************************/
global class batchDeleteSIFOrderItem implements Database.Batchable<sObject>, Schedulable{
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new batchDeleteSIFOrderItem(), 2000);
    }

    global Database.querylocator start(Database.BatchableContext BC){   
        String queryString = 'SELECT Id,Export_and_Delete__c FROM Order__c WHERE Export_and_Delete__c = true';
        return Database.getQueryLocator(queryString);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Order__c> sifList = new List<Order__c>();
        for(sObject s : scope){
            sifList.add((Order__c)s);  
        }             
        
        if (sifList.size() > 0) {
            delete sifList;
        }
    }  
   
    global void finish(Database.BatchableContext BC){     
    }
    
}
@isTest

    public class SIVStagingTrigger_Test {      
        

        static testMethod void TestMethodName() {
            
            //TODO: Create new material
            //TODO: Create new UOM
            //NOTE: Material and UOM is hard coded
  
        Test.startTest();
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        List<Custom_Product__c> parentProductList = new List<Custom_Product__c>();
        Id distributorRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            SIV_CreateInventoryLineFromStaging__c = true,
            Account_CreateWarehouseForDistributor__c = true
            );
        insert settings;
        Workflow_Rules__c wfSettings = new Workflow_Rules__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            BP_CreateInventoryRecord__c = true
            );
        insert wfSettings;
        // Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
        //     Name = 'NATIONAL',
        //     SAP_Code__c = '12345'
        //     );
        // pricingConditionList.add(nationalPricing);
        // Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
        //     Name = 'REGIONAL',
        //     SAP_Code__c = '89798'
        //     );
        // pricingConditionList.add(regionalPricing);
        // Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
        //     Name = 'SEGMENT',
        //     SAP_Code__c = '098098'
        //     );
        // pricingConditionList.add(segmentPricing);
        // insert pricingConditionList;
        Account distributorAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = distributorRecordTypeId,
            Sales_Organization__c  = 'Feeds',
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = '1234567890'
            );
        insert distributorAccount;
        
        
        
        
        
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;

        // Warehouse__c wr = new Warehouse__c();
        // wr.Account__c = distributorAccount.Id;
        // wr.Address__c = distributorAccount.Name + distributorAccount.Id;
        // wr.Warehouse_No__c = String.valueOf(distributorAccount.Id) + String.valueOf(System.now().millisecond());
        // insert wr;
        // distributorAccount.Warehouse__c = wr.Id;
        // update distributorAccount;
        
        // List<Account> accountForInsertList = new List<Account>();
        // Account acc = new Account();
        // acc.Name = 'Sample Grocery2';
        // acc.RecordTypeId = customerRecordTypeId;
        // acc.Sales_Organization__c  = 'Feeds';
        // acc.BU_Feeds__c = true;
        // acc.Branded__c = true;
        // acc.GFS__c = true;
        // acc.BU_Poultry__c = true;
        // acc.Market__c = marketList.get(0).Id;
        // acc.Distributor__c = distributorAccount.Id;
        // acc.Warehouse__c = wr.Id;
        // accountForInsertList.add(acc);
        
        // Account acc2 = new Account();
        // acc2.Name = 'Sample Grocery3';
        // acc2.RecordTypeId = customerRecordTypeId;
        // acc2.Sales_Organization__c  = 'Feeds';
        // acc2.BU_Feeds__c = true;
        // acc2.Branded__c = true;
        // acc2.GFS__c = true;
        // acc2.BU_Poultry__c = true;
        // acc2.Market__c = marketList.get(0).Id;
        // acc2.Distributor__c = distributorAccount.Id;
        // acc2.Warehouse__c = wr.Id;
        // accountForInsertList.add(acc2);
        
        // insert accountForInsertList;
        
        
        Custom_Product__c prodParent1 = new Custom_Product__c();
        prodParent1.Name = 'ParentProductFeeds';
        prodParent1.Business_Unit__c = 'Feeds';
        prodParent1.Feeds__c = true;
        prodParent1.Market__c = marketList.get(0).Id;
        prodParent1.SKU_Code__c = '111222333';
        parentProductList.add(prodParent1);
        
        Custom_Product__c prodParent2 = new Custom_Product__c();
        prodParent2.Name = 'ParentProductGFS';
        prodParent2.Business_Unit__c = 'GFS';
        prodParent2.Feeds__c = true;
        prodParent2.Market__c = marketList.get(1).Id;
        prodParent2.SKU_Code__c = '333444555';
        parentProductList.add(prodParent2);
        
        if(parentProductList.size()>0){
            System.debug('\n\n\nPARENT PRODUCT LIST : ' + parentProductList + '\n\n\n');
            insert parentProductList;
        }
        List<Conversion__c> ConversionforInsertList = new List<Conversion__c>();
        for(Custom_Product__c customProduct : parentProductList){
            Conversion__c conversionRecord = new Conversion__c(
                Name = 'KG',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1,
                Dataloader_ID__c = customProduct.SKU_Code__c + '/KG'
                );
            ConversionforInsertList.add(conversionRecord);
            Conversion__c conversionRecordPC = new Conversion__c(
                Name = 'PC',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1,
                Dataloader_ID__c = customProduct.SKU_Code__c + '/PC'
                );
            ConversionforInsertList.add(conversionRecordPC);
        }
        if(ConversionforInsertList.size()>0){
            insert ConversionforInsertList;
        }
        
        
        Inventory__c inv = [SELECT Id,Account__c, Warehouse1__c FROM Inventory__c WHERE Account__c = : distributorAccount.Id];
        List<Inventory_Item__c> invItemForInsert = new List<Inventory_Item__c>();
        Inventory_Item__c invItem  = new Inventory_Item__c(
            Inventory__c = inv.Id,
            Product__c = parentProductList[0].Id
            );
        invItemForInsert.add(invItem);
        // Inventory_Item__c invItem2  = new Inventory_Item__c(
        //     Inventory__c = inv.Id,
        //     Product__c = parentProductList[1].Id
        //     );
        // invItemForInsert.add(invItem2);
        insert invItemForInsert;
        
        Inventory_Line__c invLine = new Inventory_Line__c(
            Inventory_Item__c = invItemForInsert[0].Id,
            UOM__c = ConversionforInsertList[0].Id,
            So_No__c = '1203883109'
            );
        insert invLine;
        
        
        List<SIV_Staging__c> sivStagingList = new List<SIV_Staging__c>();
        SIV_Staging__c siv = new SIV_Staging__c();
        siv.Billing_Date__c = '20151201';
        siv.Billing_Type__c = 'S1';
        siv.BW_Doc_No__c = '106832817';
        siv.Converted_UOM__c = '';
        siv.Created_On__c = '20151223';
        siv.Customer__c = '1234567890';
        siv.Distribution_Channel__c = '10';
        siv.Division__c = '20';
        siv.Item__c = '370';
        siv.Item_Category__c = 'ZTAC';
        siv.Material__c = parentProductList[0].SKU_Code__c;
        siv.Reference_Doc_No__c = '9208066076';
        siv.Reference_Item__c = '370';
        siv.Reference_No__c = '9208066076/000370';
        siv.Sales_Document__c = '1203883109';
        siv.Sales_Org__c = '11A0';
        siv.Sales_Qty__c = '00011.080';
        siv.SIV_Value__c = '100';
        siv.UOM__c = 'KG';
        siv.COPA__c = 'test1';
        
        SIV_Staging__c siv2 = new SIV_Staging__c();
        siv2.Billing_Date__c = '20151201';
        siv2.Billing_Type__c = 'S1';
        siv2.BW_Doc_No__c = '106832817';
        siv2.Converted_UOM__c = '';
        siv2.Created_On__c = '20151223';
        siv2.Customer__c = '1234567890';
        siv2.Distribution_Channel__c = '10';
        siv2.Division__c = '20';
        siv2.Item__c = '370';
        siv2.Item_Category__c = 'ZTAC';
        siv2.Material__c = parentProductList[1].SKU_Code__c;
        siv2.Reference_Doc_No__c = '9208066076';
        siv2.Reference_Item__c = '370';
        siv2.Reference_No__c = '9208066076/000370';
        siv2.Sales_Document__c = '1203883109';
        siv2.Sales_Org__c = '11A0';
        siv2.Sales_Qty__c = '11.080-';
        siv2.UOM__c = 'KG';
        siv2.SIV_Value__c = '100-';
        siv2.COPA__c = 'test2';
        
        SIV_Staging__c siv3 = new SIV_Staging__c();
        siv3.Billing_Date__c = '20151201';
        siv3.Billing_Type__c = 'S1';
        siv3.BW_Doc_No__c = '106832817';
        siv3.Converted_UOM__c = '';
        siv3.Created_On__c = '20151223';
        siv3.Customer__c = '1234567890';
        siv3.Distribution_Channel__c = '10';
        siv3.Division__c = '20';
        siv3.Item__c = '370';
        siv3.Item_Category__c = 'ZTAC';
        siv3.Material__c = parentProductList[0].SKU_Code__c;
        siv3.Reference_Doc_No__c = '9208066076';
        siv3.Reference_Item__c = '370';
        siv3.Reference_No__c = '9208066076/000370';
        siv3.Sales_Document__c = '1203883109';
        siv3.Sales_Org__c = '11A0';
        siv3.Sales_Qty__c = '00011.080';
        siv3.SIV_Value__c = '100';
        siv3.UOM__c = 'PC';
        siv3.COPA__c = 'test3';
        
        sivStagingList.add(siv);
        sivStagingList.add(siv2);
        sivStagingList.add(siv3);
        insert sivStagingList;
        


        Test.stopTest();

    
        }
    }
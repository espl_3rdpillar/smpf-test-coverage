trigger PricingConditionDataTrigger on Pricing_Condition_Data__c (before insert,before update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            PricingConditionDataTrigger_Handler.onBeforeInsert(Trigger.new);
        }
    
        if(Trigger.isUpdate){
            PricingConditionDataTrigger_Handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
        }
    }
    
}
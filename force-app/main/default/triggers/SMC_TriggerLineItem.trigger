trigger SMC_TriggerLineItem on ePAW_Line_Item__c (before insert) 
{
    if(Trigger.isBefore && Trigger.isInsert)
    {
        // Insert Line Items for Child Requisition only.
        // As for Stand Alone Requisition, they should not be part of this trigger execution.
        SMC_TriggerLineItemHandler.ChildRequisitionLineItemsOnly(Trigger.new);
    }
}
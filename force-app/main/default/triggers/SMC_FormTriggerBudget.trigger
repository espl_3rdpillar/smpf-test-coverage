/*
    Created By: Shogo Serra 
    
    Purpose   : Scalable Match Key Criteria (Future Proof). Problem is 
    Salesforce Out-of-the-box feature cannot anymore be handled by the 
    Match Key Criteria due to Character Limit constraint.
    
    Created Date: 2017-11-27
*/
trigger SMC_FormTriggerBudget on ePAW_Budget2__c (before insert, before update) 
{
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert || Trigger.isUpdate)
        {
            System.debug('=== Executing [SMC_FormTriggerBudget] ===');
            
            // Goal: Save Match Key Criteria in field: Match_Key_Code__c
            SMC_FormEventTriggerHandlerBudget.onInsertUpdateBudgetMatchKey(Trigger.New);
            
            System.debug('>>> Terminating [SMC_FormTriggerBudget] <<<');
        }
    }
}
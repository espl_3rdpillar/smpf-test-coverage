trigger StartOfDayTrigger  on Start_of_Day__c (after insert,after update) {
    if(StaticResources.byPassStartOrEndOfDayTrigger){
       return;         
    }
    new ParseJsonForRecordDML().createRecord(Trigger.new);
}
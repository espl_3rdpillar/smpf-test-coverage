trigger CustomProductTrigger on Custom_Product__c (before Insert, before update) {
    //MODIFIED BY   : LENNARD PAUL M SANTOS(DELOITTE)
    //REASON        : ADDED TRIGGER EVENT CHECKING AND ADDED HANDLER CLASS THAT WILL HANDLE TRIGGER PROCESS.
    //DATE MODIFIED : JUN.14.2016
    //CHECK TRIGGER EVENTS
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            //ADDED CALL OF HANDLER CLASS TO PROCESS PRODUCT LIST
            CustomProductTrigger_Handler.onBeforeInsert(Trigger.new);
            List<Custom_Product__c> productList = trigger.New;
            List<Custom_Product__c> updateProductList = new List<Custom_Product__c>();
            Map<string, string> parentBUMap = new Map<string, string>();
            for (Custom_Product__c pr : productList ){
                if (pr.Parent_Product__c != null){
                    if(!parentBUMap.containsKey(pr.Parent_Product__c)){ ////parentBUMap.get(pr.Parent_Product__c) == null
                        parentBUMap.put(pr.Parent_Product__c, pr.Business_Unit__c);
                    }else{
                        String strBU;
                        if (String.isNotEmpty(parentBUMap.get(pr.Parent_Product__c))){                
                            strBU = parentBUMap.get(pr.Parent_Product__c) + ';' + pr.Business_Unit__c;
                        }else{
                            strBU = pr.Business_Unit__c;
                        }
                        parentBUMap.remove(pr.Parent_Product__c);
                        parentBUMap.put(pr.Parent_Product__c, strBU);
                    }
                    if (pr.Business_Unit__c == 'SMIS'){
                        pr.Branded__c = true;
                    }
                    if (pr.Business_Unit__c == 'GFS'){
                        pr.Branded__c = true; 
                        pr.GFS__c = true;
                    }
                    if (pr.Business_Unit__c == 'Poultry'){
                        pr.Poultry__c = true;
                    }
                    if (pr.Business_Unit__c == 'Feeds'){
                        pr.Feeds__c = true;
                    }
                }
            }
            //updateProductList = [Select Business_Unit__c, SKU_Code__c From Custom_Product__c Where Id IN :parentBUMap.keySet() ];//REMOVED AND TRANSFERED TO LOOP TO PREVENT NULL REFERENCE EXCEPTION
            for (Custom_Product__c cp : [Select Business_Unit__c, SKU_Code__c From Custom_Product__c Where Id IN :parentBUMap.keySet() ] ){
                String bu = parentBUMap.get(cp.Id);
                if(String.IsNotEmpty(cp.Business_Unit__c)){
                    cp.Business_Unit__c = cp.Business_Unit__c + ';' + bu;
                }else{
                    cp.Business_Unit__c = bu;
                }
                if(cp.Business_Unit__c.contains('SMIS') || cp.Business_Unit__c.contains('MAGNOLIA') || cp.Business_Unit__c.contains('PHC')){
                    cp.Branded__c = true;
                }
                if (cp.Business_Unit__c.contains('GFS')){
                    cp.Branded__c = true; 
                    cp.GFS__c = true;
                }
                if (cp.Business_Unit__c.contains('Poultry') || cp.Business_Unit__c.contains('Meats')){
                    cp.Poultry__c = true;
                }
                if (cp.Business_Unit__c.contains('Feeds') || cp.Business_Unit__c.contains('SMSCCI')){
                    cp.Feeds__c = true;
                }
                updateProductList.add(cp);
            }
            
            if (updateProductList.size()>0){
                update updateProductList;
            }
        }
        if(Trigger.isUpdate){
            CustomProductTrigger_Handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
        }
    }
}
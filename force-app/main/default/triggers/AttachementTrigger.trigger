trigger AttachementTrigger on Attachment (after insert, before insert, after delete) {
    if(trigger.isAfter){
        if(trigger.isInsert)
        {
            set<Id> pId = new set<Id>();
            for(Attachment att: Trigger.new){
                pId.add(att.ParentId);
            }
    		//system.assertequals(pId.size(), 99);
            System.debug('Execution 1');
            if(pId.size()>0){
                list<ePAW_Form__c> ePaw = [SELECT Id, Mandatory_Picture_Attachment__c 
                                           FROM ePAW_Form__c WHERE ID IN: pId
                                           AND (Type__c = 'DAEF' OR Type__c = 'PAW')];
                if(ePaw.size() > 0){
                    for (ePAW_Form__c e: ePaw){
                        e.Mandatory_Picture_Attachment__c = true;
                    }
                    System.debug('Execution 2');
                    update ePaw;
                }
            }
        }
        else if(trigger.isDelete)
        {
            System.debug('>>> Entering trigger.isDelete Execution Flow.');
            System.debug('LIST of Trigger Old Values: ' + trigger.old);
            System.debug('MAP of Trigger Old Values: '  + trigger.oldMap); // In this code execution we will use list because using ".values()" method will return list<object>.
            // System.debug('LIST of Trigger New Values: ' + trigger.new); // According to Salesforce Documentation, Trigger.New will not be seen in delete.
            /////
            // IS DELETED EXECUTION CODE HERE:
            /////
            Set<Id> attParentId        = new Set<Id>();
            List<Attachment> attName   = new List<Attachment>();
            List<ePAW_Form__c> attEpaw = new List<ePAW_Form__c>();
            
            // Get ParentId in trigger.old Meats
            System.debug('Values of trigger.old is : ' + trigger.old.get(0));
            // System.debug('Values of trigger.old is : ' + trigger.old.get(0).ParentId); // For Reference Only :)
            
            for(Attachment att : trigger.old)
            {
                // We need to get ParentId only.
                System.debug('ParentId of Attachment is : ' + att.ParentId);
                attParentId.add(att.ParentId);
            }
            
            // Check First if Attachments does exist.
            if(attParentId.size() != 0)
            {
                attName = [SELECT Name FROM Attachment WHERE ParentId IN :attParentId];
                
                // POST-MORTEM NOTE: In debug logs, you might see that the record has been deleted; however, if you interrupt its flow. It will rollback to its original state.
                System.debug('Double-check Values in attName List : ' + attName);
                
                // Check if ALL ATTACHMENT Files are/were deleted.
                if(attName.size() == 0)
                {
                    // If all files are/were deleted in the attachment.
                    // Update ePAW Form Mandatory Picture Attachment field into false.
                    attEpaw = [SELECT Id, Mandatory_Picture_Attachment__c FROM ePAW_Form__c WHERE Id IN :attParentId];
                    attEpaw.get(0).Mandatory_Picture_Attachment__c = false;
                    update attEpaw;
                    
                    System.debug('Mandatory Picture Attachment is set to : ' + attEpaw);
                }
            }
            /////
            // NOTHING FOLLOWS FOR IS DELETED EXECUTION CODE.
            /////
            System.debug('>>> Exiting trigger.isDelete Execution Flow.');
        }
    }
}